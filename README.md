![PlazaMalta](http://plazamalta.com.sv/wp-content/uploads/2017/08/Favicon_plazamalta.png)
# Plaza Malta Application
Aplicación para [plaza malta](http://plazamalta.com.sv) en Android implementando Kotlin(81.8%) y Java(18.2%) - LastUpdate: 06102017/10:17

## API

[PostmanFile](https://www.getpostman.com/collections/2aa6390ff385f2d0eb21)

PATH-URL: http://plazamalta.com.sv/wp-json/v1/

### END-POINTS
 
* categorias-tiendas
* tiendas/{ID}
* categorias-restaurantes
* restaurantes/{ID}
* promociones
* general
* comercio/{ID}
* eventos
* eventos/{ID}
