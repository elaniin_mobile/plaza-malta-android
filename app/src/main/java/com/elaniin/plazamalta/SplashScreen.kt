package com.elaniin.plazamalta
import android.os.Bundle
import android.os.Handler
import android.content.Intent
import com.elaniin.plazamalta.Login.Login
import android.support.v7.app.AppCompatActivity


class SplashScreen : AppCompatActivity() {
    private val SplashScreenDelay : Int = 3000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        Handler().postDelayed( {
            startActivity(Intent(this@SplashScreen, Login::class.java))
            finish()
        }, SplashScreenDelay.toLong())
    }
}