package com.elaniin.plazamalta.Ajustes


import android.Manifest.permission.CAMERA
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.support.constraint.ConstraintLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import de.hdodenhof.circleimageview.CircleImageView
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Setup.Model.User
import com.elaniin.plazamalta.Utils.Core
import java.io.File


class Ajustes : Fragment() {

    private var Vw : View ?= null
    private var u : FirebaseUser?= null
    private var firebaseAuth : FirebaseAuth ?= null
    private var firebaseAuthListener : FirebaseAuth.AuthStateListener ?= null
    private var mListener: OnFragmentInteractionListener? = null
    private var ContainerInformation : ConstraintLayout ?= null
    private var mDatabase : DatabaseReference ?= null

    private var AvatarUser : CircleImageView ?= null
    private var NewAvatarUser : CircleImageView ?= null

    private var ChangeSwitch : Switch ?= null

    private var firebaseMessaging: FirebaseMessaging? = null


    private var UriImage: String? = null

    private var UserNameText : EditText ?= null


    private val APP_DIRECTORY = "PlazaMaltaApp/"
    private val MEDIA_DIRECTORY = APP_DIRECTORY + "ProfilePicture"

    private var FLAG : Boolean = false
    private var post : User ?= null

    private var mPath: String? = null
    private val PHOTO_CODE = 200
    private val MY_PERMISSIONS = 100
    private val SELECT_PICTURE = 300

    var downloadUrl : String ?= null


    private var ChangeImageLink : TextView ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw = inflater.inflate(R.layout.fragment_ajustes, container, false)
        app().setToolbarHeader("Ajustes")

        firebaseMessaging = FirebaseMessaging.getInstance()

        UserNameText            = Vw?.findViewById(R.id.text_user_name)
        ChangeSwitch            = Vw?.findViewById(R.id.switch_change_status)
        ChangeImageLink         = Vw?.findViewById(R.id.ChangeImageLink)
        ContainerInformation    = Vw?.findViewById(R.id.ContainerInformation)
        AvatarUser              = Vw?.findViewById(R.id.mAvatarUser)

        NewAvatarUser           = Vw?.findViewById(R.id.new_avatar_user)

        val TextEmailStyled = "<u>Cambiar Fotografía</u>"
        ChangeImageLink?.setText(Html.fromHtml(TextEmailStyled), TextView.BufferType.SPANNABLE)

        val RecoveryPassword = Vw?.findViewById<Button>(R.id.button_recovery_password)
        val SaveInformation  = Vw?.findViewById<Button>(R.id.button_save_information)

        firebaseAuth = FirebaseAuth.getInstance()

        u = firebaseAuth!!.currentUser
        if (u != null) {
            u?.providerData!!
                    .filter { it.providerId == "password" }
                    .forEach { PasswordSection(Vw!!) }
            mDatabase = FirebaseDatabase.getInstance().reference.child("users").child(u?.uid)
            val postListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    post = dataSnapshot.getValue<User>(User::class.java)
                    UserNameText?.setText(post?.username)
                    Glide.with(activity).load(post?.photo).into(AvatarUser)
                }
                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(context, "Failed to load post. $databaseError", Toast.LENGTH_SHORT).show()
                }
            }
            mDatabase?.addValueEventListener(postListener)

            RecoveryPassword!!.setOnClickListener {
                val S = AlertDialog.Builder(context)
                S.setMessage("¿Seguro que deseas restaurar tu contraseña? Se te enviará un mensaje a tu cuenta de correo electrónico.")
                S.setCancelable(false)
                S.setPositiveButton("SI", { _, _ ->
                    val auth = FirebaseAuth.getInstance()
                    val emailAddress = post?.email
                    auth.sendPasswordResetEmail(emailAddress!!).addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d("SEND MESSAGE", "Email sent.")
                        } else { }
                    }
                })
                S.setNegativeButton("NO", { dialog, which -> dialog.cancel() })
                val AlertSendData = S.create()
                AlertSendData.show()
            }




            SaveInformation?.setOnClickListener {
                val S = AlertDialog.Builder(context)
                S.setMessage("¿Seguro que deseas actualizar tu información de usuario?")
                S.setCancelable(false)
                S.setPositiveButton("SI", { dialog, which ->

                    if(!UserNameText?.text.toString().isNullOrEmpty() && UriImage != null) { //Si ambos estan completos
                        val StorageFirebase = FirebaseStorage.getInstance()
                        val ReferenceStorage = StorageFirebase.getReferenceFromUrl("gs://plazamalta-app.appspot.com")
                        val Req : UploadTask = ReferenceStorage.child("ProfilePicture/${FirebaseAuth.getInstance().currentUser!!.uid}/photo.jpg").putFile(Uri.parse(UriImage))
                        Req.addOnFailureListener(OnFailureListener {
                            // Handle unsuccessful uploads
                        }).addOnSuccessListener(OnSuccessListener<UploadTask.TaskSnapshot> { taskSnapshot ->
                            val childUpdates = HashMap<String, Any>()
                            childUpdates.put("username", UserNameText?.text.toString())
                            childUpdates.put("photo",taskSnapshot.downloadUrl.toString())
                            this.mDatabase?.updateChildren(childUpdates)
                        })
                        Toast.makeText(context, "¡Se actualizaron tus datos!", Toast.LENGTH_SHORT).show()
                    }


                    else if(!UserNameText?.text.toString().isNullOrEmpty() && UriImage == null){ //Si hay nombre de usuario y no hay foto
                        val childUpdates = HashMap<String, Any>()
                        childUpdates.put("username", UserNameText?.text.toString())
                        this.mDatabase?.updateChildren(childUpdates)
                        Toast.makeText(context, "¡Se actualizó tu nombre de usuario!", Toast.LENGTH_SHORT).show()



                    }else if(UserNameText?.text.toString().isNullOrEmpty() && UriImage != null) { //Si no hay nombre de usuario y hay foto
                        val StorageFirebase = FirebaseStorage.getInstance()
                        val ReferenceStorage = StorageFirebase.getReferenceFromUrl("gs://plazamalta-app.appspot.com")
                        val Req : UploadTask = ReferenceStorage.child("ProfilePicture/${FirebaseAuth.getInstance().currentUser!!.uid}/photo.jpg").putFile(Uri.parse(UriImage))
                        val childUpdates = HashMap<String, Any>()
                        Req.addOnFailureListener(OnFailureListener {
                            // Handle unsuccessful uploads
                        }).addOnSuccessListener(OnSuccessListener<UploadTask.TaskSnapshot> { taskSnapshot ->
                            downloadUrl = taskSnapshot.downloadUrl.toString()
                            childUpdates.put("photo",downloadUrl!!)
                        })
                        this.mDatabase?.updateChildren(childUpdates)
                        Toast.makeText(context, "¡Se actualizaron tu fotografía de perfil!", Toast.LENGTH_SHORT).show()



                    }else if(UserNameText?.text.toString().isNullOrEmpty() && UriImage == null) { //Si ambos estan vacíos
                        Toast.makeText(context, "No se encontró información para actualizar.", Toast.LENGTH_SHORT).show()
                    }


                })
                S.setNegativeButton("NO", { dialog, which -> dialog.cancel() })
                val AlertSendData = S.create()
                AlertSendData.show()
            }

        }


        firebaseAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            u = firebaseAuth.currentUser
            if (u != null) {





            }else{ }
        }


        if(mayRequestStoragePermission()) {
            ChangeImageLink?.isEnabled = true
        }

        ChangeImageLink?.setOnClickListener {
            ShowOptions()
        }

        ChangeSwitch?.isChecked = app().getPreference(this.context!!, "subscribedTo_general").equals("true")
        ChangeSwitch?.setOnCheckedChangeListener({ compoundButton, subscribe -> subscribeToTopic(subscribe, "general") })

        return Vw
    }


    private fun mayRequestStoragePermission(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true
        if (ActivityCompat.checkSelfPermission(this.activity!!, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity!!, CAMERA) == PackageManager.PERMISSION_GRANTED)
            return true
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE) || shouldShowRequestPermissionRationale(CAMERA)) {
            requestPermissions(arrayOf(WRITE_EXTERNAL_STORAGE, CAMERA), MY_PERMISSIONS)
        } else {
            requestPermissions(arrayOf(WRITE_EXTERNAL_STORAGE, CAMERA), MY_PERMISSIONS)
        }
        return false
    }


    private fun OpenCamera() {
        val file = File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY)
        var isDirectoryCreated = file.exists()
        if (!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs()
        if (isDirectoryCreated) {
            val TimesTampResource = System.currentTimeMillis() / 1000
            val ResourceName = TimesTampResource.toString() + ".jpg"
            mPath = "${Environment.getExternalStorageDirectory()}${File.separator}$MEDIA_DIRECTORY${File.separator}$ResourceName"
            val newFile = File(mPath)
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile))
            startActivityForResult(intent, PHOTO_CODE)
        }
    }

    private fun ShowOptions() {
        val option = arrayOf<CharSequence>("Tomar fotografía", "Elegir de galeria", "Cancelar")
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Elige una opción")
        builder.setItems(option) { dialog, which ->
            if (option[which] === "Tomar fotografía") {
                OpenCamera()
            } else if (option[which] === "Elegir de galeria") {
                var xintent: Intent? = null
                xintent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                xintent.type = "image/*"
                startActivityForResult(Intent.createChooser(xintent, "SELECCIONA UNA OPCIÓN."), SELECT_PICTURE)
            } else {
                dialog.dismiss()
            }
        }
        builder.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                PHOTO_CODE -> {
                    MediaScannerConnection.scanFile(Vw?.context, arrayOf(mPath!!), null) { path, uri ->
                        Log.i("ExternalStorage", "Scanned $path:")
                        Log.i("ExternalStorage", "-> Uri = " + uri)
                        UriImage = uri.toString()
                    }
                    AvatarUser?.visibility = View.GONE
                    NewAvatarUser?.visibility = View.VISIBLE
                    Glide.with(activity).load(mPath).into(NewAvatarUser)
                }
                SELECT_PICTURE -> {
                    val path = data!!.data
                    UriImage = path.toString()
                    AvatarUser?.visibility = View.GONE
                    NewAvatarUser?.visibility = View.VISIBLE
                    Glide.with(activity).load(path).into(NewAvatarUser)
                }
            }
        }
    }




    fun PasswordSection(Vw : View){
        ContainerInformation?.visibility = View.VISIBLE
    }



    override fun onStart(){
        super.onStart()
        firebaseAuth?.addAuthStateListener(firebaseAuthListener!!)
    }

    override fun onStop(){
        super.onStop()
        if(firebaseAuthListener != null){
            firebaseAuth?.removeAuthStateListener(firebaseAuthListener!!)
        }
    }

    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    fun app() : Core {
        return Core.instance!!
    }


    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        fun newInstance(): Ajustes {
            return Ajustes()
        }
    }


    fun subscribeToTopic(subscribe: Boolean, topic: String) {
        if (subscribe) {
            firebaseMessaging?.subscribeToTopic(topic)
        } else {
            firebaseMessaging?.unsubscribeFromTopic(topic)
        }
        app().setPreference(this.context!!, "subscribedTo_" + topic,
                if (subscribe) "true" else "false")
    }



}
