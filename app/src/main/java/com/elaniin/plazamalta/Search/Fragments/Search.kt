package com.elaniin.plazamalta.Search.Fragments

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.*
import org.json.JSONObject


class Search : Fragment(), SearchView.OnQueryTextListener {

    var Vw : View ?= null
    var rSearchView : RecyclerView ?= null
    var SearchAdater : commerceAdapter ?= null
    var itemList : ArrayList<commerceModel> = ArrayList()
    var ProgressBarSearch : ProgressBar ?= null
    var logo_actionbar : LinearLayout ?= null

    var Srch : ConstraintLayout ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw                  = inflater!!.inflate(R.layout.fragment_search, container, false)
        rSearchView         = Vw?.findViewById(R.id.RecyclerViewPromociones)
        ProgressBarSearch   = Vw?.findViewById(R.id.ProgressBarLoaderSearch)
        Srch                = Vw?.findViewById(R.id.Srch)

        PlazaMalta().setToolbarHeader("")
        ProgressBarSearch?.visibility = View.VISIBLE

        val lm = LinearLayoutManager(activity)
        rSearchView!!.layoutManager = lm

        Srch?.isFocusable = true
        Srch?.isFocusableInTouchMode = true


        InitializeService()

        return Vw
    }

    fun PlazaMalta() : Core {
        return Core.instance!!
    }

    fun InitializeService(){

        itemList = ArrayList()

        rSearchView?.visibility = View.VISIBLE

        val service = ServiceRequest()
        itemList.clear()
        val ApplicationC = ApplicationController(service)
        ApplicationC.Get(Core.TAG_GET_ALL) { response ->
            if (response != null) {
                (0..response.length() - 1).forEach { t ->
                    val SearchElements : JSONObject = response.getJSONObject(t)
                    //Set de variables según el orden del json para el modelo: Tiendas.
                    val Horarios = SearchElements.getJSONObject(Core.TAG_HORARIOS)
                    itemList.add(  commerceModel(
                            SearchElements.optString(Core.TAG_ID),
                            SearchElements.optString(Core.TAG_TIPO),
                            SearchElements.optString(Core.TAG_TITULO),
                            SearchElements.optString(Core.TAG_DESCRIPCION_CORTA),
                            SearchElements.optString(Core.TAG_LOGO),
                            SearchElements.optString(Core.TAG_DESCRIPCION),
                            SearchElements.optString(Core.TAG_WEBSITE),
                            SearchElements.optString(Core.TAG_EMAIL),
                            SearchElements.optString(Core.TAG_FACEBOOK),
                            SearchElements.optString(Core.TAG_TWITTER),
                            SearchElements.optString(Core.TAG_INSTAGRAM),
                            SearchElements.optString(Core.TAG_TELEFONO),
                            SearchElements.optString(Core.TAG_TELEFONO_2),
                            SearchElements.optString(Core.TAG_WHATSAPP),
                            SearchElements.optString(Core.TAG_MENU_PDF),
                            Horarios.optString(Core.TAG_A_LUNES),
                            Horarios.optString(Core.TAG_C_LUNES),
                            Horarios.optString(Core.TAG_A_MARTES),
                            Horarios.optString(Core.TAG_C_MARTES),
                            Horarios.optString(Core.TAG_A_MIERCOLES),
                            Horarios.optString(Core.TAG_C_MIERCOLES),
                            Horarios.optString(Core.TAG_A_JUEVES),
                            Horarios.optString(Core.TAG_C_JUEVES),
                            Horarios.optString(Core.TAG_A_VIERNES),
                            Horarios.optString(Core.TAG_C_VIERNES),
                            Horarios.optString(Core.TAG_A_SABADO),
                            Horarios.optString(Core.TAG_C_SABADO),
                            Horarios.optString(Core.TAG_A_DOMINGO),
                            Horarios.optString(Core.TAG_C_DOMINGO)))

                }

                SearchAdater = commerceAdapter(activity!!, itemList)
                rSearchView!!.adapter = SearchAdater
                SearchAdater?.notifyDataSetChanged()
                ProgressBarSearch?.visibility = View.GONE

            } else {

                if(Core().connectionExists(context!!)){
                    Toast.makeText(context, context!!.getString(R.string.ToastMessageNoConnection), Toast.LENGTH_LONG).show()
                }else{

                }

                ProgressBarSearch?.visibility = View.GONE

            }
        }
    }





        @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateOptionsMenu(menu : Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val i = menu.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(i) as SearchView
        searchView.setOnQueryTextListener(this)

        //searchView.maxWidth = Integer.MAX_VALUE

        MenuItemCompat.setOnActionExpandListener(i,
                object : MenuItemCompat.OnActionExpandListener {
                    override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                        SearchAdater?.setFilter(itemList)
                        Core.instance?.VisibilityLogo(true)
                        return true // Return true to collapse action view
                    }
                    override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                        Core.instance?.VisibilityLogo(false)
                        return true // Return true to expand action view
                    }
                })
    }


    override fun onQueryTextSubmit(query: String?): Boolean { return false }
    override fun onQueryTextChange(newText: String?): Boolean {
        val filteredModeList : ArrayList<commerceModel> = filter(itemList, newText!!)
        SearchAdater?.setFilter(filteredModeList)
        return false
    }
    private fun filter(models: ArrayList<commerceModel>, query: String): ArrayList<commerceModel> {
        var query = query
        query = query.toLowerCase()
        val filteredModelList = ArrayList<commerceModel>()
        for (model : commerceModel  in models) {
            val text = model.Titulo.toLowerCase()
            if (text.contains(query)) {
                filteredModelList.add(model)
            }
        }
        return filteredModelList
    }









    companion object {
        fun newInstance(): Search {
            return Search()
        }
    }

}