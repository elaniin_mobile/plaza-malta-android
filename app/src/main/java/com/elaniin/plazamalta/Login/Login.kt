package com.elaniin.plazamalta.Login

import android.content.Intent
import android.content.Context

import android.graphics.Rect
import android.net.Uri
import android.os.Bundle

import android.support.v7.app.AppCompatActivity
import android.support.constraint.ConstraintLayout

import android.view.View
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager

import android.widget.*

import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.Core
import com.elaniin.plazamalta.Setup.Setup
import com.elaniin.plazamalta.MainActivity
import com.elaniin.plazamalta.Setup.Model.User

import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton

import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

import com.google.firebase.auth.*
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.auth.FirebaseAuth.AuthStateListener

import java.util.regex.Matcher
import java.util.regex.Pattern

import kotlinx.android.synthetic.main.activity_login.*


class Login : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    var callbackManager             : CallbackManager   ?= null
    var UserLogin                   : EditText          ?= null
    var GoogleButton                : Button            ?= null//ConventionalButton
    var PasswordLogin               : EditText          ?= null
    var FacebookButton              : Button            ?= null//ConventionalButton
    var MessageLoginHeader          : TextView          ?= null
    var ButtonGoogleSignIn          : SignInButton      ?= null//SigInButton
    var ButtonFacebookSignIn        : LoginButton       ?= null//SigInButton

    var mProgressBarLogin           : ProgressBar       ?= null

    var ContLogin                   : ConstraintLayout  ?= null

    var usr : FirebaseUser? = null

    private var mAuth                   : FirebaseAuth? = null
    private var FirebaseAuthListener    : AuthStateListener? = null
    private var mGoogleApiClient        : GoogleApiClient? = null

    private var RC_SIGN_IN = 9001

    private var mDatabase : DatabaseReference       ?= null
    private var FirebaseMssagng : FirebaseMessaging ?= null

    private var post : User ?= null

    private var TAG_URL_VIDEO   : String = "android.resource://com.elaniin.plazamalta" + "/raw/" + "video_login"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        InitActivityElements()

        callbackManager = CallbackManager.Factory.create()
        // ConfigureGoogleSignIn
        var gso: GoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                requestIdToken(getString(R.string.google_server_client_id)).requestEmail().build()

        mGoogleApiClient = GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build()
        // ConfigureFacebookSigIn
        ButtonFacebookSignIn!!.setReadPermissions("email", "public_profile")
        ButtonFacebookSignIn!!.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(LoginR: LoginResult) { handleFacebookAccessToken(LoginR.accessToken) }
            override fun onCancel() {mProgressBarLogin?.visibility = View.GONE}
            override fun onError(Error: FacebookException) {mProgressBarLogin?.visibility = View.GONE} //"Error: ${Error.message}!"
        })
        mAuth = FirebaseAuth.getInstance()
        FirebaseAuthListener = AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                SessionActive()
            }
        }

        SetProperties()
        SetLatoFont()
    }


    fun InitActivityElements(){

        ContLogin               = findViewById(R.id.ContLogin)
        UserLogin               = findViewById(R.id.UserLogin)
        GoogleButton            = findViewById(R.id.GoogleButton)
        PasswordLogin           = findViewById(R.id.PasswordLogin)
        FacebookButton          = findViewById(R.id.FacebookButton)
        mProgressBarLogin       = findViewById(R.id.progressBarLogin)
        MessageLoginHeader      = findViewById(R.id.MessageLoginHeader)
        ButtonGoogleSignIn      = findViewById(R.id.ButtonGoogleSignIn)
        ButtonFacebookSignIn    = findViewById(R.id.ButtonFacebookSignIn)

        ContLogin?.isFocusable = true
        ContLogin?.isFocusableInTouchMode = true

        FirebaseMssagng     = FirebaseMessaging.getInstance()
    }

    fun PlazaMalta() : Core {
        return Core.instance!!
    }

    fun SetLatoFont(){
        PlazaMalta().setLatoFont(GoogleButton!!,        Core.REGULAR_FONT_TYPE)
        PlazaMalta().setLatoFont(FacebookButton!!,      Core.REGULAR_FONT_TYPE)
        PlazaMalta().setLatoFont(MessageLoginHeader!!,  Core.REGULAR_FONT_TYPE)

    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun signIn() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onStart() {
        super.onStart()
        mAuth?.addAuthStateListener(FirebaseAuthListener!!)
    }

    override fun onStop() {
        super.onStop()
        mAuth?.removeAuthStateListener(FirebaseAuthListener!!)
    }

    public override fun onResume() {
        super.onResume()
        VideoViewBackground.resume()
    }

    public override fun onPause() {
        super.onPause()
        VideoViewBackground.suspend()
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        var credential: AuthCredential = FacebookAuthProvider.getCredential(token.token)
        mAuth?.signInWithCredential(credential)?.addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                usr = task.result.user
                val user: FirebaseUser = mAuth?.currentUser!!
                mProgressBarLogin?.visibility = View.GONE
                updateUI(user)
            } else {
                // If sign in fails, display a message to the user.
                Toast.makeText(this@Login, "Authentication failed.", Toast.LENGTH_SHORT).show()
                mProgressBarLogin?.visibility = View.GONE
                updateUI(null!!)
            }
        }
    }


    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_UP) {
            val view = currentFocus
            if (view != null) {
                val consumed = super.dispatchTouchEvent(ev)
                val viewTmp = currentFocus
                val viewNew = viewTmp ?: view
                if (viewNew.equals(view)) {
                    val rect = Rect()
                    val coordinates = IntArray(2)
                    view.getLocationOnScreen(coordinates)
                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.width, coordinates[1] + view.height)
                    val x = ev.x.toInt()
                    val y = ev.y.toInt()
                    if (rect.contains(x, y)) {
                        return consumed
                    }
                } else if (viewNew is EditText) {
                    return consumed
                }
                val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(viewNew.windowToken, 0)
                viewNew.clearFocus()
                return consumed
            }
        }
        return super.dispatchTouchEvent(ev)
    }



    private fun updateUI(u: FirebaseUser) {
        var v : Boolean = false
        var y : String ?= null
        if (true) {
            for (profile in u?.providerData!!) {
                if (profile.providerId == "password"){
                    v = true
                } else{
                    v = false
                    y = u.displayName
                }
            }

            if(v){
                mDatabase = FirebaseDatabase.getInstance().reference.child("users").child(u?.uid)
                val postListener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        post = dataSnapshot.getValue<User>(User::class.java)
                        //Toast.makeText(this@Login, """${post?.username} ${getString(R.string.MessageLoginStart)}""", Toast.LENGTH_SHORT).show()
                        FirebaseMessaging.getInstance().subscribeToTopic("general")
                    }
                    override fun onCancelled(databaseError: DatabaseError) {}
                }
                mDatabase?.addValueEventListener(postListener)
                mProgressBarLogin?.visibility = View.GONE
            }else{
                //Toast.makeText(this@Login, "$y, ${getString(R.string.MessageLoginStart)}", Toast.LENGTH_SHORT).show()
                mProgressBarLogin?.visibility = View.GONE
            }

        }

    }



        fun SessionActive() {
            var i = Intent(this@Login, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
            super.onActivityResult(requestCode, resultCode, data)
            callbackManager?.onActivityResult(requestCode, resultCode, data)

            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode === RC_SIGN_IN) {
                var result: GoogleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                FirebasehandleSignInResultGoogle(result.signInAccount)
            }


        }



        private fun FirebasehandleSignInResultGoogle(result: GoogleSignInAccount?) {
            if(result != null){
                var cred: AuthCredential = GoogleAuthProvider.getCredential(result?.idToken, null)
                mAuth?.signInWithCredential(cred)?.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        usr = task.result.user
                        val user = mAuth?.currentUser
                        updateUI(user!!)
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(this@Login, "Authentication failed.", Toast.LENGTH_SHORT).show()
                        //updateUI(null!!)
                    }
                }
            }

        }


        fun SetProperties() {
            val MC = MediaController(this)
            VideoViewBackground?.setOnPreparedListener({ mp -> mp.isLooping = true })
            VideoViewBackground?.setVideoURI(Uri.parse(TAG_URL_VIDEO))
            VideoViewBackground!!.start()
            VideoViewBackground.requestFocus()

            ButtonEmailSignIn?.setOnClickListener {
                startActivity(Intent(this@Login, Setup::class.java))
                finish()
            }
            LoginButton?.setOnClickListener {

                mProgressBarLogin?.visibility = View.VISIBLE

                if(PlazaMalta().connectionExists(applicationContext)) {
                    val matcherObj: Matcher = Pattern.compile(Core.EmailValidation).matcher(UserLogin?.text.toString())
                    when {
                        (UserLogin?.text.isNullOrEmpty() || PasswordLogin?.text.isNullOrEmpty()) -> {
                            Toast.makeText(this@Login, getString(R.string.MessageValidBlankUserLoginText), Toast.LENGTH_LONG).show()
                            mProgressBarLogin?.visibility = View.GONE
                        }
                        (!matcherObj.matches()) -> {
                            Toast.makeText(this@Login, getString(R.string.MessageEmailTextIncorrect), Toast.LENGTH_LONG).show()
                            mProgressBarLogin?.visibility = View.GONE
                        }
                        (PasswordLogin?.text?.length!! <= 8) -> {
                            Toast.makeText(this@Login, "${UserLogin?.text.toString()}: ${getString(R.string.MessageValidPasswordLengthLogin)}", Toast.LENGTH_LONG).show()
                            mProgressBarLogin?.visibility = View.GONE
                        }
                        else -> {
                            mAuth?.signInWithEmailAndPassword(UserLogin?.text.toString(), PasswordLogin?.text.toString())?.addOnCompleteListener(this) { task ->
                                if (task.isSuccessful) {
                                    usr = task.result.user
                                    val user: FirebaseUser = FirebaseAuth.getInstance().currentUser!!
                                    mProgressBarLogin?.visibility = View.GONE
                                    updateUI(user!!)
                                } else {
                                    mProgressBarLogin?.visibility = View.GONE
                                    Toast.makeText(this@Login, "${UserLogin?.text.toString()}${getString(R.string.MessageLoginIncorrect)}", Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                    }
                }else{
                    mProgressBarLogin?.visibility = View.GONE
                    Toast.makeText(this@Login, getString(R.string.MessageNoConnection), Toast.LENGTH_SHORT).show()
                }

            }

        }

        fun onClick(v: View) {
            if(PlazaMalta().connectionExists(applicationContext)) {
                mProgressBarLogin?.visibility = View.VISIBLE
                if (v === FacebookButton) {
                    ButtonFacebookSignIn?.performClick()
                } else if (v === GoogleButton) {
                    signIn()
                }
            }else{
                mProgressBarLogin?.visibility = View.GONE
                Toast.makeText(this@Login, getString(R.string.MessageNoConnection), Toast.LENGTH_SHORT).show()
            }
        }
    }