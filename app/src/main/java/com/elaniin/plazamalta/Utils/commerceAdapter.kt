package com.elaniin.plazamalta.Utils

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.elaniin.plazamalta.R
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Alelopez07_ on 31/10/2017.
 */

class commerceAdapter(private val ContextActivity: Context, commerceList: ArrayList<commerceModel>) : RecyclerView.Adapter<commerceAdapter.VwH>() {

    private var Vw: View? = null
    private var RM: commerceModel? = null
    private var commerceList = ArrayList<commerceModel>()
    private var FLAG = false

    init {
        this.commerceList = commerceList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): commerceAdapter.VwH {
        Vw = LayoutInflater.from(parent.context).inflate(R.layout.commerce_item, parent, false)
        return VwH(Vw!!)
    }

    private fun PlazaMalta(): Core {
        return Core.instance!!
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: commerceAdapter.VwH, x: Int) {
        if (x < commerceList.size) {
            val RX = commerceList[x]
            RM = commerceList[x]
            holder.ShortDescription.text = RM!!.Description


            val f = SimpleDateFormat("HH", Locale.ENGLISH)
            val hour = f.format(Date())

            val calendar = Calendar.getInstance()
            val hourOfDay = calendar.get(Calendar.HOUR_OF_DAY)

            val D = SimpleDateFormat("EEEE", Locale.ENGLISH).format(System.currentTimeMillis())
            val date_api_string = SimpleDateFormat("HH:MM", Locale.ENGLISH)
            val date_api_final = SimpleDateFormat("HH", Locale.ENGLISH)

            when (D) {
                "Monday"    -> {
                    if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.lunes_a))) < 0){
                        holder.time_information.text = "Abre a las: ${RX.lunes_a} hrs."
                        FLAG = true
                    }

                    else if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.lunes_c))) > 0){
                        holder.time_information.text = "Abre a las: ${RX.martes_a} hrs."
                        FLAG = true
                    }

                    else{
                        FLAG = false
                    }
                }


                "Tuesday"   -> {
                    if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.martes_a))) < 0){
                        holder.time_information.text = "Abre a las: ${RX.martes_a} hrs."
                        FLAG = true
                    }

                    else if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.martes_c))) > 0){
                        holder.time_information.text = "Abre a las: ${RX.miercoles_a} hrs."
                        FLAG = true
                    }else{
                        FLAG = false
                    }

                }



                "Wednesday" -> {
                    if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.miercoles_a))) < 0){
                        holder.time_information.text = "Abre a las: ${RX.miercoles_a} hrs."
                        FLAG = true
                    }

                    else if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.miercoles_c))) > 0){
                        holder.time_information.text = "Abre a las: ${RX.jueves_a} hrs."
                        FLAG = true
                    }else{
                        FLAG = false
                    }
                }

                "Thursday"  -> {
                    if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.jueves_a))) < 0){
                        holder.time_information.text = "Abre a las: ${RX.jueves_a} hrs."
                        FLAG = true
                    }

                    else if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.jueves_c))) > 0){
                        holder.time_information.text = "Abre a las: ${RX.viernes_a} hrs."
                        FLAG = true
                    }else{
                        FLAG = false
                    }
                }

                "Friday"    -> {
                    if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.viernes_a))) < 0){
                        holder.time_information.text = "Abre a las: ${RX.viernes_a} hrs."
                        FLAG = true
                    }

                    else if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.viernes_c))) > 0){
                        holder.time_information.text = "Abre a las: ${RX.sabado_a} hrs."
                        FLAG = true
                    }else{
                        FLAG = false
                    }
                }

                "Saturday"  -> {
                    if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.sabado_a))) < 0){
                        holder.time_information.text = "Abre a las: ${RX.sabado_a} hrs."
                        FLAG = true
                    }

                    else if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.sabado_c))) > 0){
                        holder.time_information.text = "Abre a las: ${RX.domingo_a} hrs."
                        FLAG = true
                    }else{
                        FLAG = false
                    }
                }

                "Sunday"    -> {
                    if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.domingo_a))) < 0){
                        holder.time_information.text = "Abre a las: ${RX.domingo_a} hrs."
                        FLAG = true
                    }

                    else if(hour.compareTo(date_api_final.format(date_api_string.parse(RX.domingo_c))) > 0){
                        holder.time_information.text = "Abre a las: ${RX.lunes_a} hrs."
                        FLAG = true
                    }else{
                        FLAG = false
                    }
                }
                else -> {
                    holder.time_information.text = "CERRADO"
                    holder.time_icon.setColorFilter(Color.parseColor("#FF2626"))
                }
            }

            when(FLAG) {
                false -> {
                    holder.time_information.text = "ABIERTO"
                    holder.time_information.setTextColor(Color.parseColor("#228c00"))
                    holder.time_icon.setColorFilter(Color.parseColor("#228c00"))
                }
                true -> {
                    holder.time_information.setTextColor(Color.parseColor("#FF2626"))
                    holder.time_icon.setColorFilter(Color.parseColor("#FF2626"))
                }
            }

            val iLoader = CustomV.getInstance(ContextActivity).imageLoader

            iLoader[RX.Asset, ImageLoader.getImageListener(holder.HeaderItem,R.drawable.header_tiendas,android.R.drawable.ic_dialog_alert),1080,700]
            holder.HeaderItem.setImageUrl(RX.Asset, iLoader)

            holder.CommerceButton.setOnClickListener {
                val FM = (ContextActivity as FragmentActivity).supportFragmentManager
                val IR = commerceInformation.newInstance(RX)
                val FT = FM.beginTransaction()
                FT.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                FT.add(android.R.id.content, IR).addToBackStack(null).commit()
            }

        }
    }

    override fun getItemCount(): Int {
        if (true)
            return commerceList.size
        else
            return 0
    }

    fun setFilter(mpK: ArrayList<commerceModel>) {
        commerceList = ArrayList<commerceModel>()
        commerceList.addAll(mpK)
        notifyDataSetChanged()
    }

    inner class VwH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val HeaderItem      : NetworkImageView  = itemView.findViewById(R.id.HeaderItem)
        val CommerceButton  : Button            = itemView.findViewById(R.id.CommerceButton)
        val time_icon       : ImageView         = itemView.findViewById(R.id.time_icon)
        val ShortDescription: TextView          = itemView.findViewById(R.id.ShortDescription)
        val time_information: TextView          = itemView.findViewById(R.id.time_information)

        init {
            SetLatoFont()
        }

        private fun SetLatoFont() {
            PlazaMalta().setLatoFont(ShortDescription, Core.REGULAR_FONT_TYPE)
        }
    }
}
