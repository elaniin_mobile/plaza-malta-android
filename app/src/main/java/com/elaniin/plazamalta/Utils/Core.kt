package com.elaniin.plazamalta.Utils

/**
 * Created by elaniin on 5/9/2017.
 */

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.text.SpannableString
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.Volley
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.elaniin.plazamalta.Eventos.Models.EventosModel
import com.elaniin.plazamalta.Promociones.Models.PromocionesModel
import com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
import com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric


class Core : Application(){

    private var mRequestQueue: RequestQueue? = null
    private var mImageLoader: ImageLoader? = null
    private var onTitleChange: Response.Listener<Array<Any>>? = null
    private var logoVisibility : Response.Listener<Boolean> ?= null
    private var onFragmentChange: Response.Listener<Fragment>? = null
    private var onAddClicked: Response.Listener<Boolean>? = null
    private var logoActionBar : Response.Listener<Boolean> ?= null
    private var onFragmentLocationReset: Response.Listener<Boolean>? = null

    private val APP_NAME = "PlazaMalta"
    private val timeoutTime = 30


    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        instance = this
    }

    fun log(tag: String?, txt: String?) {
        var tag = tag
        var txt = txt
        tag = if (tag == null) "null" else tag
        txt = if (txt == null) "null" else txt
        Log.println(Log.ASSERT, tag, txt)
    }

    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) { return Volley.newRequestQueue(applicationContext) }
            return field
        }

    val imageLoader: ImageLoader
        get() {
            requestQueue
            if (mImageLoader == null) {
                mImageLoader = ImageLoader(this.mRequestQueue, LruBitmapCache())
            }
            return this.mImageLoader!!
        }




    fun <T> addToRequestQueue(req: Request<T>?, tag: String) {
        if (req != null) {
            req.retryPolicy = DefaultRetryPolicy(timeoutTime * 1000, DEFAULT_MAX_RETRIES,
                    DEFAULT_BACKOFF_MULT)
            req.tag = if (TextUtils.isEmpty(tag)) APP_NAME else tag
            requestQueue?.add(req)
        }
    }

    fun <T> addToRequestQueue(req: Request<T>?) {
        if (req != null) {
            req.retryPolicy = DefaultRetryPolicy(timeoutTime * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            req.tag = APP_NAME
            requestQueue?.add(req)
        }
    }




    fun cancelPendingRequests(tag: Any) {
        if (requestQueue != null) {
            requestQueue!!.cancelAll(tag)
        }
    }


    fun setOnTitleChange(onTitleChange: Response.Listener<Array<Any>>) {
        this.onTitleChange = onTitleChange
    }


    fun setlogoVisibility(logoVisibility : Response.Listener<Boolean>){
        this.logoVisibility = logoVisibility
    }


    fun VisibilityLogo(Flag : Boolean){
        this.logoVisibility?.onResponse(Flag)
    }


    fun setOnFragmentChange(onFragmentChange: Response.Listener<Fragment>) {
        this.onFragmentChange = onFragmentChange
    }

    fun setOnAddClicked(onAddClicked: Response.Listener<Boolean>) {
        this.onAddClicked = onAddClicked
    }

    fun setOnFragmentLocationReset(onFragmentLocationReset: Response.Listener<Boolean>) {
        this.onFragmentLocationReset = onFragmentLocationReset
    }


    fun setToolbarHeader(title: String, hasAdd: Boolean) {
        onTitleChange?.onResponse(arrayOf(false, title, hasAdd))
    }

    fun setToolbarHeader(drawable: Int) {
        onTitleChange?.onResponse(arrayOf(true, drawable, false))
    }

    fun setFragment(F: Fragment) {
        onFragmentChange?.onResponse(F)
    }

    fun doAddClick() {
        onAddClicked?.onResponse(true)
    }

    fun setMainFragment() {
        onFragmentLocationReset?.onResponse(true)
    }


    fun setToolbarHeader(title: String) {
        onTitleChange?.onResponse(arrayOf(false, title, false))
    }




    fun setLatoFont(v: View, type: String) {
        val lato = Typeface.createFromAsset(v.context.assets, "fonts/Lato-$type.ttf")
        if (v is TextView) {
            v.typeface = lato
        } else if (v is EditText) {
            v.typeface = lato
        } else if (v is Button) {
            val btn = v as Button
            btn.typeface = lato
        } else if (v is NavigationView) {
            val m = (v as NavigationView).menu
            for (i in 0..m.size() - 1) {
                val mi = m.getItem(i)
                val subMenu = mi.subMenu
                if (subMenu != null && subMenu.size() > 0) {
                    for (j in 0..subMenu.size() - 1) {
                        val smi = subMenu.getItem(j)
                        val ss = SpannableString(smi.title)
                        smi.title = ss
                    }
                }

                val latoUnique = Typeface
                        .createFromAsset(v.getContext().getAssets(), "fonts/Lato-Black.ttf")
                val ss = SpannableString(mi.title)
                mi.title = ss
            }
        }
    }


    private fun getSharedPreferences(c: Context): SharedPreferences {
        return c.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
    }

    fun getPreference(c: Context, key: String): String {
        return getSharedPreferences(c).getString(key, "")
    }

    fun setPreference(c: Context, key: String, value: String) {
        getSharedPreferences(c).edit().putString(key, value).apply()
        log("setPreference", key + " -> " + value)
    }

    fun clearPreferences(c: Context) {
        val e = getSharedPreferences(c).edit()
        e.clear()
        e.commit()
        log("ClearPreferences", "Preferences cleared.")
    }




    fun connectionExists(c: Context): Boolean {
        val conMgr = c.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val i = conMgr.activeNetworkInfo ?: return false
        if (!i.isConnected)
            return false
        if (!i.isAvailable)
            return false
        return true
    }

    companion object {

        private val TAG = Core::class.java.simpleName
        @get:Synchronized var instance: Core? = null
            private set

        var EventosList         : ArrayList<EventosModel>       = ArrayList()
        var PromocionesList     : ArrayList<PromocionesModel>   = ArrayList()

        var EmailValidation : String        = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

        //val TAG_PATH_URL                  = "http://toolboxsv.com/dev/plaza_malta/wp-json/v1/" -> DevPath
        val TAG_PATH_URL                    = "http://plazamalta.com.sv/wp-json/v1/"

        val TAG_GET_ALL                     = "all"

        //{EndPointsTAG URL Request.}
        val TAG_WIFI                        = "general"
        val TAG_TIENDAS                     = "tiendas/"
        val TAG_CATEGORIAS_TIENDAS          = "categorias-tiendas"
        val TAG_CATEGORIAS_RESTAURANTES     = "categorias-restaurantes"
        val TAG_RESTAURANTES                = "restaurantes/"
        val TAG_PROMOCIONES                 = "promociones/0"
        val TAG_EVENTOS_URL                 = "eventos"

        val BOLD_FONT_TYPE                  = "Bold"
        val REGULAR_FONT_TYPE               = "Regular"

        val TAG_ID                          = "id"
        val TAG_TITULO                      = "titulo"
        val TAG_DESCRIPCION                 = "descripcion"
        val TAG_IMAGEN                      = "imagen"
        val TAG_FECHA                       = "fecha"

        val TAG_TIPO                        = "tipo"

        val TAG_COMERCIO                    = "comercio"

        val TAG_DESCRIPCION_CORTA           = "descripcion_corta"
        val TAG_LOGO                        = "logo"
        val TAG_WEBSITE                     = "website"
        val TAG_EMAIL                       = "email"
        val TAG_FACEBOOK                    = "facebook"
        val TAG_TWITTER                     = "twitter"
        val TAG_INSTAGRAM                   = "instagram"
        val TAG_TELEFONO                    = "telefono"
        val TAG_TELEFONO_2                  = "telefono_2"
        val TAG_WHATSAPP                    = "whatsapp"
        val TAG_MENU_PDF                    = "menu"

        val TAG_HORARIOS                    = "horarios"

        val TAG_A_LUNES                     = "apertura_lunes"
        val TAG_A_MARTES                    = "apertura_martes"
        val TAG_A_MIERCOLES                 = "apertura_miercoles"
        val TAG_A_JUEVES                    = "apertura_jueves"
        val TAG_A_VIERNES                   = "apertura_viernes"
        val TAG_A_SABADO                    = "apertura_sabado"
        val TAG_A_DOMINGO                   = "apertura_domingo"

        val TAG_C_LUNES                     = "cierre_lunes"
        val TAG_C_MARTES                    = "cierre_martes"
        val TAG_C_MIERCOLES                 = "cierre_miercoles"
        val TAG_C_JUEVES                    = "cierre_jueves"
        val TAG_C_VIERNES                   = "cierre_viernes"
        val TAG_C_SABADO                    = "cierre_sabado"
        val TAG_C_DOMINGO                   = "cierre_domingo"

    }
}