package com.elaniin.plazamalta.Utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    private DatePickerDialog.OnDateSetListener onDateSetListener;

    private int year;
    private int month;
    private int day;

    public DatePickerFragment(){}

    public void setOnDateSetListener(DatePickerDialog.OnDateSetListener onDateSetListener) {
        this.onDateSetListener = onDateSetListener;
    }

    public void setDate(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void setDate(Long date){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date);
        this.year = c.get(c.YEAR);
        this.month =  c.get(c.MONTH);
        this.day = c.get(c.DAY_OF_MONTH);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), this.onDateSetListener, this.year, this.month, this.day);
        return dpd;
    }
}