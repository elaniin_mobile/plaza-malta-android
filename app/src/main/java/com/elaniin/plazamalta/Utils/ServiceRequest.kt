package com.elaniin.plazamalta.Utils

import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.StringRequest
import org.json.JSONArray

class ServiceRequest : ServiceInterface {

    val TAG = ServiceRequest::class.java.simpleName
    override fun Get(path: String, completionHandler: (response: JSONArray?) -> Unit) {
        val request = object : StringRequest(Request.Method.GET, Core.TAG_PATH_URL + path,
                Response.Listener<String> { response ->
                    completionHandler(JSONArray(response))
                    Log.d(TAG, "/GET request OK! Response: $response")
                }, Response.ErrorListener { error ->
                    completionHandler(null)
                    VolleyLog.e(TAG, "/GET request fail! Error: ${error.message}")
        }
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }
        Core.instance?.addToRequestQueue(request,TAG)
    }
}