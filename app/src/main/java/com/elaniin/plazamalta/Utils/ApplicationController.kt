package com.elaniin.plazamalta.Utils

import org.json.JSONArray

/**
 * Created by elaniin on 11/9/2017.
 */

class ApplicationController constructor(serviceInjection: ServiceInterface) : ServiceInterface{
    private val service : ServiceInterface = serviceInjection
    override fun Get(path: String, completionHandler: (response: JSONArray?) -> Unit) {
        service.Get(path, completionHandler)
    }

}