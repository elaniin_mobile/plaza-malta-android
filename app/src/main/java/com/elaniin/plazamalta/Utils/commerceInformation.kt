package com.elaniin.plazamalta.Utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.constraint.ConstraintLayout
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTransaction
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Restaurantes.Fragments.Menu
import kotlinx.android.synthetic.main.commerce_information.*

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Alelopez07_ on 31/10/2017.
 */


class commerceInformation : DialogFragment() {

    private var Vw : View ?= null
    private var HeaderImageInfo  :   NetworkImageView ?= null

    private var txtTimeInformation      :   TextView  ?= null

    private var EmailTAG                :   TextView  ?= null
    private var EmailValue              :   TextView  ?= null
    private var email_section           :   LinearLayout ?= null

    private var IgValue                 :   ImageButton ?= null
    private var TwitterValue            :   ImageButton ?= null
    private var FacebookValue           :   ImageButton ?= null
    private var socialNetworkTAG        :   TextView  ?= null
    private var social_network_section  :   LinearLayout ?= null

    private var phone_section           :   LinearLayout ?= null
    private var TelefonoTAG             :   TextView  ?= null
    private var txtTelefono1            :   TextView  ?= null
    private var txtTelefono2            :   TextView  ?= null

    private var monday      : TextView ?= null
    private var tuesday     : TextView ?= null
    private var wednesday   : TextView ?= null
    private var thursday    : TextView ?= null
    private var friday      : TextView ?= null
    private var saturday    : TextView ?= null
    private var sunday      : TextView ?= null
    private var mAccessM    : TextView ?= null

    private var mLinerarMenu   : LinearLayout ?= null

    private var CommerceX                       :   commerceModel ?= null
    private var CloseDialog                     :   ImageButton  ?= null
    private var ContainerCloseDiag              :   ConstraintLayout ?= null

    var TAG = commerceInformation::class.java.simpleName!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw = inflater!!.inflate(R.layout.commerce_information, container, false)
        if (arguments != null) {
            InitFragmentElements(Vw!!)
            CommerceX = arguments!!.getParcelable(ARG_PARAM_CX)
            BaseFragment(CommerceX!!)
        }
        return Vw
    }


    fun InitFragmentElements(Vw : View){
        txtTimeInformation         = Vw.findViewById(R.id.txtTimeInformation)

        EmailTAG                = Vw.findViewById(R.id.EmailTAG)
        EmailValue              = Vw.findViewById(R.id.EmailValue)
        email_section           = Vw.findViewById(R.id.email_section)

        TelefonoTAG             = Vw.findViewById(R.id.TelefonoTAG)
        txtTelefono1            = Vw.findViewById(R.id.txtTelefono1)
        txtTelefono2            = Vw.findViewById(R.id.txtTelefono2)
        phone_section           = Vw.findViewById(R.id.phone_section)

        monday                  = Vw.findViewById(R.id.monday)
        tuesday                 = Vw.findViewById(R.id.tuesday)
        wednesday               = Vw.findViewById(R.id.wednesday)
        thursday                = Vw.findViewById(R.id.thursday)
        friday                  = Vw.findViewById(R.id.friday)
        saturday                = Vw.findViewById(R.id.saturday)
        sunday                  = Vw.findViewById(R.id.sunday)

        mAccessM                = Vw.findViewById(R.id.MenuTAG)

        mLinerarMenu            = Vw.findViewById(R.id.menu_section)

        IgValue                 = Vw.findViewById(R.id.InstagramLink)
        TwitterValue            = Vw.findViewById(R.id.TwitterLink)
        FacebookValue           = Vw.findViewById(R.id.FacebookLink)
        socialNetworkTAG       = Vw.findViewById(R.id.socialNetworkTAG)

        HeaderImageInfo     = Vw.findViewById(R.id.HeaderImageInfo)
        CloseDialog         = Vw.findViewById(R.id.CloseDialog)
        ContainerCloseDiag  = Vw.findViewById(R.id.ContainerCloseDiag)

    }

    @SuppressLint("SetTextI18n")
    fun BaseFragment(CX: commerceModel){

        val IL = CustomV.getInstance(Vw?.context).imageLoader

        Log.i("MenuPDF ->", CX.menu_pdf)


        if(CX.tipo == "restaurante"){
            Log.i("TIPO -> ", "RESTAURANTE")
            Log.i("MENU -> ", CX.menu_pdf)
            mLinerarMenu?.visibility = View.VISIBLE

            if(CX.menu_pdf.isNullOrEmpty()){
                mLinerarMenu?.visibility = View.GONE
            }else{
                mAccessM?.setOnClickListener {
                    val FM = (context as FragmentActivity).supportFragmentManager
                    val IR = Menu.newInstance(CX.menu_pdf)
                    val FT = FM.beginTransaction()
                    FT.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    FT.add(android.R.id.content, IR).addToBackStack(null).commit()
                }
            }
        }else{
            Log.i("TIPO -> ", "TIENDA")
            mLinerarMenu?.visibility = View.GONE
        }


        //SetHeaderAsset
        IL[CX.Asset, ImageLoader.getImageListener(HeaderImageInfo,R.drawable.header_tiendas,android.R.drawable.ic_dialog_alert),1080,700]
        HeaderImageInfo?.setImageUrl(CX.Asset, IL)
        CloseDialog?.setOnClickListener { dismiss() }
        ContainerCloseDiag?.setOnClickListener { dismiss() }

        //SetTimeInformation
        val D = SimpleDateFormat("EEEE", Locale.ENGLISH).format(System.currentTimeMillis())
        when (D) {
            "Monday"    -> txtTimeInformation?.text = "ABIERTO HASTA LAS ${CX.lunes_c}"
            "Tuesday"   -> txtTimeInformation?.text = "ABIERTO HASTA LAS ${CX.martes_c}"
            "Wednesday" -> txtTimeInformation?.text = "ABIERTO HASTA LAS ${CX.miercoles_c}"
            "Thursday"  -> txtTimeInformation?.text = "ABIERTO HASTA LAS ${CX.jueves_c}"
            "Friday"    -> txtTimeInformation?.text = "ABIERTO HASTA LAS ${CX.viernes_c}"
            "Saturday"  -> txtTimeInformation?.text = "ABIERTO HASTA LAS ${CX.sabado_c}"
            "Sunday"    -> txtTimeInformation?.text = "ABIERTO HASTA LAS ${CX.domingo_c}"
            else        -> txtTimeInformation?.text = "CERRADO"
        }

        if(CX.telefono_1 == "" && CX.telefono_2 == ""){ phone_section?.visibility = View.GONE }

        if(CX.telefono_1 == ""){ txtTelefono1?.visibility = View.GONE } else {
            val TextSecondPhoneStyled = "<u>${CX.telefono_1}</u>"
            txtTelefono1?.setText(Html.fromHtml(TextSecondPhoneStyled), TextView.BufferType.SPANNABLE)
            txtTelefono1?.setOnClickListener { startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + txtTelefono1?.text))) }
        }

        if(CX.telefono_2 == ""){ txtTelefono2?.visibility = View.GONE } else {
            val TextSecondPhoneStyled = "<u>${CX.telefono_2}</u>"
            txtTelefono2?.setText(Html.fromHtml(TextSecondPhoneStyled), TextView.BufferType.SPANNABLE)
            txtTelefono2?.setOnClickListener { startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + txtTelefono2?.text))) }
        }

        if(CX.Email == ""){email_section?.visibility = View.GONE}else{
            EmailValue?.text = CX.Email
            val TextEmailStyled = "<u>${CX.Email}</u>"
            EmailValue?.setText(Html.fromHtml(TextEmailStyled), TextView.BufferType.SPANNABLE)
            EmailValue?.setOnClickListener {
                val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", CX.Email, null))
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.MessageSubject))
                val MailerIntent = Intent.createChooser(emailIntent, getString(R.string.SendMailtoMessage))
                context!!.startActivity(MailerIntent)
            }
        }

        //SocialNetworkValidation
        if(CX.InstagramValue == "") {
            IgValue?.visibility = View.GONE
        } else {
            IgValue?.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(CX.InstagramValue)))
            }
        }
        if(CX.FacebookValue == "") {
            FacebookValue?.visibility = View.GONE
        } else {
            FacebookValue?.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(CX.FacebookValue)))
            }
        }
        if(CX.TwitterValue == "") {
            TwitterValue?.visibility = View.GONE
        } else {
            TwitterValue?.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(CX.TwitterValue)))
            }
        }

        monday?.text        = "${CX.lunes_a} - ${CX.lunes_c}"
        tuesday?.text       = "${CX.martes_a} - ${CX.martes_c}"
        wednesday?.text     = "${CX.miercoles_a} - ${CX.miercoles_c}"
        thursday?.text      = "${CX.jueves_a} - ${CX.jueves_c}"
        friday?.text        = "${CX.viernes_a} - ${CX.viernes_c}"
        saturday?.text      = "${CX.sabado_a} - ${CX.sabado_c}"
        sunday?.text        = "${CX.domingo_a} - ${CX.domingo_c}"

        SetLatoFont()

    }

    fun PlazaMalta() : Core {
        return Core.instance!!
    }

    fun SetLatoFont(){
        PlazaMalta().setLatoFont(EmailTAG!!,            Core.BOLD_FONT_TYPE)
        PlazaMalta().setLatoFont(TelefonoTAG!!,         Core.BOLD_FONT_TYPE)
        PlazaMalta().setLatoFont(txtTimeInformation!!,  Core.BOLD_FONT_TYPE)
        PlazaMalta().setLatoFont(socialNetworkTAG!!,    Core.BOLD_FONT_TYPE)
        PlazaMalta().setLatoFont(txtTelefono1!!,        Core.REGULAR_FONT_TYPE)
        PlazaMalta().setLatoFont(txtTelefono2!!,        Core.REGULAR_FONT_TYPE)
        PlazaMalta().setLatoFont(EmailValue!!,          Core.REGULAR_FONT_TYPE)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
        return dialog
    }

    companion object {
        private val ARG_PARAM_CX = "CX"
        fun newInstance(CX : Parcelable): commerceInformation {
            val IFR = commerceInformation()
            val AR = Bundle()
            AR.putParcelable(ARG_PARAM_CX,CX)
            IFR.arguments = AR
            return IFR
        }
    }
}