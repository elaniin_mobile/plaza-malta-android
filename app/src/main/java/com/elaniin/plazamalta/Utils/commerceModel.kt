package com.elaniin.plazamalta.Utils

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Alelopez07_ on 26/10/2017.
 */

data class commerceModel (val ID                : String,
                          val tipo              : String,
                          val Titulo            : String,
                          val ShortDescription  : String,
                          val Asset             : String,
                          val Description       : String,
                          val Website           : String,
                          val Email             : String,
                          val FacebookValue     : String,
                          val TwitterValue      : String,
                          val InstagramValue    : String,
                          val telefono_1        : String,
                          val telefono_2        : String,
                          val whatsapp          : String,
                          val menu_pdf          : String,
                          val lunes_a           : String,
                          val lunes_c           : String,
                          val martes_a          : String,
                          val martes_c          : String,
                          val miercoles_a       : String,
                          val miercoles_c       : String,
                          val jueves_a          : String,
                          val jueves_c          : String,
                          val viernes_a         : String,
                          val viernes_c         : String,
                          val sabado_a          : String,
                          val sabado_c          : String,
                          val domingo_a         : String,
                          val domingo_c         : String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ID)
        parcel.writeString(tipo)
        parcel.writeString(Titulo)
        parcel.writeString(ShortDescription)
        parcel.writeString(Asset)
        parcel.writeString(Description)
        parcel.writeString(Website)
        parcel.writeString(Email)
        parcel.writeString(FacebookValue)
        parcel.writeString(TwitterValue)
        parcel.writeString(InstagramValue)
        parcel.writeString(telefono_1)
        parcel.writeString(telefono_2)
        parcel.writeString(whatsapp)
        parcel.writeString(menu_pdf)
        parcel.writeString(lunes_a)
        parcel.writeString(lunes_c)
        parcel.writeString(martes_a)
        parcel.writeString(martes_c)
        parcel.writeString(miercoles_a)
        parcel.writeString(miercoles_c)
        parcel.writeString(jueves_a)
        parcel.writeString(jueves_c)
        parcel.writeString(viernes_a)
        parcel.writeString(viernes_c)
        parcel.writeString(sabado_a)
        parcel.writeString(sabado_c)
        parcel.writeString(domingo_a)
        parcel.writeString(domingo_c)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<commerceModel> {
        override fun createFromParcel(parcel: Parcel): commerceModel {
            return commerceModel(parcel)
        }

        override fun newArray(size: Int): Array<commerceModel?> {
            return arrayOfNulls(size)
        }
    }
}