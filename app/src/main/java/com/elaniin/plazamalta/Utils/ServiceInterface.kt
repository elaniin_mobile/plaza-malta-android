package com.elaniin.plazamalta.Utils
import org.json.JSONArray
/**
 * Created by elaniin on 11/9/2017.
 */
interface ServiceInterface {
    fun Get(path: String, completionHandler : (response : JSONArray?) -> Unit)


}