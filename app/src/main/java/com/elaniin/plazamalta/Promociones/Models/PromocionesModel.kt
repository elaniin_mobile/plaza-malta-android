package com.elaniin.plazamalta.Promociones.Models

import android.os.Parcel
import android.os.Parcelable




/**
 * Created by elaniin on 14/9/2017.
 */


data class PromocionesModel(val Comercio : String, val TituloPromocion : String, val DescripcionTitulo : String, val PromocionImage : String) : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(Comercio)
        parcel.writeString(TituloPromocion)
        parcel.writeString(DescripcionTitulo)
        parcel.writeString(PromocionImage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PromocionesModel> {
        override fun createFromParcel(parcel: Parcel): PromocionesModel {
            return PromocionesModel(parcel)
        }

        override fun newArray(size: Int): Array<PromocionesModel?> {
            return arrayOfNulls(size)
        }
    }
}