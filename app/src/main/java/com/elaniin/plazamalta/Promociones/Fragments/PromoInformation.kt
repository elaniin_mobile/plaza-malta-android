package com.elaniin.plazamalta.Promociones.Fragments

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.DialogFragment
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.CustomV

class PromoInformation : DialogFragment() {

    private var Vw  : View ?= null

    private var HeaderPromoImage            : NetworkImageView  ?= null
    private var CloseDialogPromo            : ConstraintLayout       ?= null
    private var CloseDialogPromoInfo        : ImageView?= null
    private var TituloPromocionInfo         : TextView          ?= null
    private var TextPromoDescription        : TextView          ?= null

    private var TitlePromotion          : String ?= null
    private var ImagePromotion          : String ?= null
    private var DescriptionPromotion    : String ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw = inflater!!.inflate(R.layout.fragment_promo_information, container, false)

        if (arguments != null) {
            InitFragmentElements()
            BaseFragment()
        }

        return Vw
    }

    fun InitFragmentElements(){
        HeaderPromoImage        = Vw?.findViewById(R.id.HeaderPromoImage)
        CloseDialogPromo        = Vw?.findViewById(R.id.CloseDialogPromo)
        CloseDialogPromoInfo    = Vw?.findViewById(R.id.CloseDialogPromoInfo)
        TituloPromocionInfo     = Vw?.findViewById(R.id.TituloPromocionInfo)
        TextPromoDescription    = Vw?.findViewById(R.id.TextPromoDescription)
    }

    fun BaseFragment(){
        TitlePromotion          = arguments?.getString(ARG_FIRST_PARAM)
        ImagePromotion          = arguments?.getString(ARG_SECOND_PARAM)
        DescriptionPromotion    = arguments?.getString(ARG_THIRD_PARAM)

        CloseDialogPromo?.setOnClickListener { dismiss() }
        CloseDialogPromoInfo?.setOnClickListener { dismiss() }

        HeaderPromoImage!!.setImageURI(Uri.parse(ImagePromotion))

        val IL = CustomV.getInstance(Vw?.context).imageLoader
        IL[ImagePromotion, ImageLoader.getImageListener(HeaderPromoImage,R.drawable.header_tiendas,android.R.drawable.ic_dialog_alert), 1080, 700]
        HeaderPromoImage?.setImageUrl(ImagePromotion, IL)

        TituloPromocionInfo?.text   = TitlePromotion
        TextPromoDescription?.text  = DescriptionPromotion
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(false)
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL)
        dialog.window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED)
        dialog.setCancelable(false)
        return dialog
    }

    companion object {
        private val ARG_FIRST_PARAM     = "title"
        private val ARG_SECOND_PARAM    = "image"
        private val ARG_THIRD_PARAM     = "description"

        fun newInstance(TitlePromotion : String, ImagePromotion : String, DescriptionPromotion : String): PromoInformation {
            val PromoFragment = PromoInformation()
            val a = Bundle()
            a.putString(ARG_FIRST_PARAM,    TitlePromotion)
            a.putString(ARG_SECOND_PARAM,   ImagePromotion)
            a.putString(ARG_THIRD_PARAM,    DescriptionPromotion)
            PromoFragment.arguments = a
            return PromoFragment
        }
    }
}