package com.elaniin.plazamalta.Promociones.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.elaniin.plazamalta.Promociones.Adapters.PromocionesAdapter
import com.elaniin.plazamalta.Promociones.Models.PromocionesModel
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.ApplicationController
import com.elaniin.plazamalta.Utils.Core
import com.elaniin.plazamalta.Utils.ServiceRequest
import com.elaniin.plazamalta.Utils.SpacesItemDecoration
import org.json.JSONObject




class Promociones : Fragment() {

    private var Vw : View ?= null
    private var PromocionesList = Core.PromocionesList

    private var PAdapter                : PromocionesAdapter ?= null
    private var ProgressBarLoader       : ProgressBar ?= null
    private var RecyclerViewPromociones : RecyclerView ?= null

    private var PromocionesContext      : FragmentActivity ?= null
    private var mListener               : OnFragmentInteractionListener? = null

    private var mBlankState             : ConstraintLayout  ?= null
    private var InfoConnection          : ConstraintLayout  ?= null
    private var AppBarHeaderPromociones : AppBarLayout ?= null

    var TAG = Promociones::class.java.simpleName!!

    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw = inflater!!.inflate(R.layout.fragment_promociones, container, false)
        InitializeFragment()
        GetPromociones()
        return Vw
    }

    fun PlazaMalta() : Core {
        return Core.instance!!
    }

    fun InitializeFragment(){
        setHasOptionsMenu(true)
        PlazaMalta().setToolbarHeader("Promociones")

        mBlankState                 = Vw?.findViewById(R.id.BlankStatePromotions)
        InfoConnection              = Vw?.findViewById(R.id.InfoConnection)
        ProgressBarLoader           = Vw?.findViewById(R.id.ProgressBarLoader)
        RecyclerViewPromociones     = Vw?.findViewById(R.id.RecyclerViewPromociones)
        AppBarHeaderPromociones     = Vw?.findViewById(R.id.AppBarHeaderPromociones)

        ProgressBarLoader?.visibility = View.VISIBLE

        RecyclerViewPromociones?.setHasFixedSize(true)
        RecyclerViewPromociones?.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        val decoration = SpacesItemDecoration(4)
        RecyclerViewPromociones?.addItemDecoration(decoration)
    }

    @SuppressLint("MissingSuperCall")
    override fun onAttach(context: Context?) {
        PromocionesContext = context as FragmentActivity
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    fun GetPromociones(){

        AppBarHeaderPromociones?.visibility     = View.VISIBLE

        val Service = ServiceRequest()
        PromocionesList.clear()
        val AController = ApplicationController(Service)
        AController.Get(Core.TAG_PROMOCIONES){response ->

            if(response?.length() == 0){
                mBlankState?.visibility = View.VISIBLE
            }

            if(response != null){
                for(p in 0..response.length() -1){
                    val PromoJsonRequest : JSONObject = response.getJSONObject(p)

                    val TAG_TITULO_PROMOCION        = PromoJsonRequest.getString(Core.TAG_TITULO)
                    val TAG_DESCRIPCION_PROMOCION   = PromoJsonRequest.getString(Core.TAG_DESCRIPCION)
                    val TAG_IMAGEN_PROMOCION        = PromoJsonRequest.getString(Core.TAG_IMAGEN)
                    val TAG_COMERCIO_PROMOCION      = PromoJsonRequest.getString(Core.TAG_COMERCIO)

                    PromocionesList.add(PromocionesModel(TAG_COMERCIO_PROMOCION,TAG_TITULO_PROMOCION,TAG_DESCRIPCION_PROMOCION,TAG_IMAGEN_PROMOCION))
                }

                RecyclerViewPromociones?.visibility     = View.VISIBLE

                PAdapter = PromocionesAdapter(activity, PromocionesList)
                RecyclerViewPromociones?.adapter = PAdapter
                PAdapter?.notifyDataSetChanged()

                ProgressBarLoader?.visibility = View.GONE

            }else{

                PlazaMalta().cancelPendingRequests(TAG)

                if(PlazaMalta().connectionExists(PromocionesContext!!)){
                    Toast.makeText(PromocionesContext, context?.getString(R.string.ToastMessageNoConnection), Toast.LENGTH_LONG).show()
                }else{
                    mBlankState?.visibility                 = View.VISIBLE
                    InfoConnection?.visibility              = View.VISIBLE
                    ProgressBarLoader?.visibility           = View.GONE
                    RecyclerViewPromociones?.visibility     = View.GONE
                    AppBarHeaderPromociones?.visibility     = View.GONE
                }

            }
        }
    }

    override fun onStop() {
        super.onStop()
        PlazaMalta().cancelPendingRequests(TAG)
    }
    override fun onPause() {
        super.onPause()
    }
    companion object {
        fun newInstance(): Promociones { return Promociones() }
    }
}