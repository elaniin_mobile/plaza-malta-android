package com.elaniin.plazamalta.Promociones.Adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.widget.RecyclerView;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.view.LayoutInflater;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import com.elaniin.plazamalta.R;
import com.elaniin.plazamalta.Utils.CustomV;
import com.elaniin.plazamalta.Promociones.Models.PromocionesModel;
import com.elaniin.plazamalta.Promociones.Fragments.PromoInformation;

/**
 * Created by elaniin on 14/9/2017.
 */

public class PromocionesAdapter extends RecyclerView.Adapter<PromocionesAdapter.VwH> {

    private View Vw;
    private PromocionesModel P;
    private Context ContextActivity;
    private ArrayList<PromocionesModel> PromocionesList = new ArrayList<>();

    public PromocionesAdapter(Context ContextActivity, ArrayList<PromocionesModel> PromocionesList){
        this.ContextActivity = ContextActivity;
        this.PromocionesList = PromocionesList;
    }

    @Override
    public PromocionesAdapter.VwH onCreateViewHolder(ViewGroup parent, int viewType) {
        Vw = LayoutInflater.from(parent.getContext()).inflate(R.layout.promociones_item, parent, false);
        return new VwH(Vw);
    }

    @Override
    public void onBindViewHolder(PromocionesAdapter.VwH holder, int x) {
        final PromocionesModel PromoPosition = PromocionesList.get(x);

        ImageLoader iLoader = CustomV.getInstance(ContextActivity).getImageLoader();
        iLoader.get(PromoPosition.getPromocionImage(), iLoader.getImageListener(holder.PromocionImage, R.drawable.header_tiendas, android.R.drawable.ic_dialog_alert));
        holder.PromocionImage.setImageUrl(PromoPosition.getPromocionImage(),iLoader);

        holder.TituloPromocion.setText(PromoPosition.getTituloPromocion());
        holder.MessagePromocion.setText(PromoPosition.getDescripcionTitulo());
    }

    @Override
    public int getItemCount() {
        return PromocionesList.size();
    }

    public class VwH extends RecyclerView.ViewHolder {

        TextView            TituloPromocion;
        TextView            MessagePromocion;
        NetworkImageView    PromocionImage;

        public VwH(View iVw) {
            super(iVw);

            TituloPromocion     = (TextView) iVw.findViewById(R.id.TituloPromocion);
            PromocionImage      = (NetworkImageView) iVw.findViewById(R.id.PromocionImagen);
            MessagePromocion    = (TextView) iVw.findViewById(R.id.MessagePromocion);


            iVw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int x = getAdapterPosition();
                    if(x != RecyclerView.NO_POSITION){
                        FragmentManager FM = ((FragmentActivity)ContextActivity).getSupportFragmentManager();
                        PromoInformation E = PromoInformation.Companion.newInstance(PromocionesList.get(getAdapterPosition()).getTituloPromocion(),
                                PromocionesList.get(getAdapterPosition()).getPromocionImage(),PromocionesList.get(getAdapterPosition()).getDescripcionTitulo());
                        FragmentTransaction FT = FM.beginTransaction();
                        FT.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        FT.add(android.R.id.content, E).addToBackStack(null).commit();
                    }
                }
            });


        }
    }

}