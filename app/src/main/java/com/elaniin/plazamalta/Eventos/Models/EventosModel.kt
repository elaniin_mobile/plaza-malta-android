package com.elaniin.plazamalta.Eventos.Models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by elaniin on 14/9/2017.
 */

data class EventosModel(val EventoID : Int, val TituloEvento : String, val DescripcionEvento : String, val EventoImage : String, val FechaEvento : String) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(EventoID)
        parcel.writeString(TituloEvento)
        parcel.writeString(DescripcionEvento)
        parcel.writeString(EventoImage)
        parcel.writeString(FechaEvento)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventosModel> {
        override fun createFromParcel(parcel: Parcel): EventosModel {
            return EventosModel(parcel)
        }

        override fun newArray(size: Int): Array<EventosModel?> {
            return arrayOfNulls(size)
        }
    }

}