package com.elaniin.plazamalta.Eventos.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.elaniin.plazamalta.Eventos.Adapters.EventosAdapter
import com.elaniin.plazamalta.Eventos.Models.EventosModel
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.ApplicationController
import com.elaniin.plazamalta.Utils.Core
import com.elaniin.plazamalta.Utils.ServiceRequest
import org.json.JSONObject

class Eventos : Fragment() {

    private var Vw : View ?= null

    private var EAdapter : EventosAdapter ?= null
    private var ListaEventos = Core.EventosList
    private var EventosContext : FragmentActivity ?= null


    private var mListener               : OnFragmentInteractionListener ?= null
    private var InfoConnection          : ConstraintLayout              ?= null
    private var mBlankState             : ConstraintLayout              ?= null
    private var RecyclerViewEvents      : RecyclerView                  ?= null
    private var ProgressBarLoaderEvents : ProgressBar                   ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        PlazaMalta().setToolbarHeader("Eventos")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw = inflater.inflate(R.layout.fragment_eventos, container, false)

        InitFragmentElements()
        GetData()

        return Vw
    }


    fun InitFragmentElements(){
        mBlankState                     = Vw?.findViewById(R.id.BlankStateEvents)
        InfoConnection                  = Vw?.findViewById(R.id.InfoConnection)
        RecyclerViewEvents              = Vw?.findViewById(R.id.RecyclerViewEvents)
        ProgressBarLoaderEvents         = Vw?.findViewById(R.id.ProgressBarLoaderEvents)

        ProgressBarLoaderEvents?.visibility = View.VISIBLE

        RecyclerViewEvents?.setHasFixedSize(true)
        val LManager = LinearLayoutManager(activity)
        RecyclerViewEvents?.layoutManager = LManager
        LManager.isAutoMeasureEnabled = false
    }


    fun GetData(){

        val s = ServiceRequest()
        ListaEventos.clear()
        val AC = ApplicationController(s) //InstanceApplicationController
        AC.Get(Core.TAG_EVENTOS_URL){ response ->

            if(response?.length() == 0){
                mBlankState?.visibility = View.VISIBLE
            }

            if(response != null){
                for(e in 0..response.length() -1){

                    val EventJsonRequest : JSONObject = response.getJSONObject(e)

                    val TAG_ID          = EventJsonRequest.getInt(Core.TAG_ID)
                    val TAG_TITLE       = EventJsonRequest.getString(Core.TAG_TITULO)
                    val TAG_DESCRIPTION = EventJsonRequest.getString(Core.TAG_DESCRIPCION)
                    val TAG_IMAGE_URL   = EventJsonRequest.getString(Core.TAG_IMAGEN)
                    val TAG_DATE        = EventJsonRequest.getString(Core.TAG_FECHA)

                    ListaEventos.add(EventosModel(TAG_ID,TAG_TITLE,TAG_DESCRIPTION,TAG_IMAGE_URL,TAG_DATE))

                }

                RecyclerViewEvents?.visibility = View.VISIBLE

                EAdapter = EventosAdapter(activity, ListaEventos)
                RecyclerViewEvents?.adapter = EAdapter
                EAdapter?.notifyDataSetChanged()

                ProgressBarLoaderEvents?.visibility = View.GONE

            }else{

                ProgressBarLoaderEvents?.visibility = View.GONE

                if(!PlazaMalta().connectionExists(EventosContext!!)){
                    mBlankState?.visibility = View.GONE
                    InfoConnection?.visibility = View.VISIBLE
                    RecyclerViewEvents?.visibility = View.GONE
                }

            }
        }
    }


    fun PlazaMalta() : Core {
        return Core.instance!!
    }


    @SuppressLint("MissingSuperCall")
    override fun onAttach(context: Context?) {
        EventosContext = context as FragmentActivity
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        fun newInstance(): Eventos = Eventos()
    }

}