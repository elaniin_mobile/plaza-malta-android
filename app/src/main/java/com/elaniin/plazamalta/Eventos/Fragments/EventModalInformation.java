package com.elaniin.plazamalta.Eventos.Fragments;

import java.util.Date;
import android.app.Dialog;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import android.view.View;
import android.view.Window;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import android.view.WindowManager;
import android.widget.TextView;
import android.widget.ImageButton;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.elaniin.plazamalta.R;
import com.elaniin.plazamalta.Utils.CustomV;

/**
 * Created by elaniin on 25/9/2017.
 */

public class EventModalInformation extends DialogFragment{

    View                Vw;
    TextView            FechaEventoInformacion;
    TextView            TituloEventoInformacion;
    TextView            DescripcionEventoInformacion;
    ImageButton         CloseDialog;
    NetworkImageView    HeaderEventoImage;

    Date DateInformation;

    private String ImageEv;
    private String TitleEv;
    private String DateEv;
    private String DescriptionEv;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {super.onCreate(savedInstanceState);}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Vw = inflater.inflate(R.layout.fragment_evento_informacion, container, false);

        if(getArguments() != null) {
            InitFragmentElements();
            BaseFragment();
        }
        return Vw;
    }

    void InitFragmentElements(){
        CloseDialog                     = (ImageButton)         Vw.findViewById(R.id.CloseDialog);
        HeaderEventoImage               = (NetworkImageView)    Vw.findViewById(R.id.HeaderEventoImage);
        FechaEventoInformacion          = (TextView)            Vw.findViewById(R.id.FechaEventoInformacion);
        TituloEventoInformacion         = (TextView)            Vw.findViewById(R.id.TituloEventoInformacion);
        DescripcionEventoInformacion    = (TextView)            Vw.findViewById(R.id.DescripcionEventoInformacion);
    }

    void BaseFragment(){

        ImageEv         = getArguments().getString("image");
        TitleEv         = getArguments().getString("title");
        DateEv          = getArguments().getString("date");
        DescriptionEv   = getArguments().getString("description");

        CloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        try {
            DateInformation = new SimpleDateFormat("yyyy-MM-dd").parse(DateEv);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }

        String FormatDateInformation = new SimpleDateFormat("EEE, MMM dd yyyy").format(DateInformation);

        ImageLoader IL = CustomV.getInstance(Vw.getContext()).getImageLoader();
        IL.get(ImageEv, ImageLoader.getImageListener(HeaderEventoImage, R.drawable.header_tiendas, android.R.drawable.ic_dialog_alert));

        HeaderEventoImage.setImageUrl(ImageEv,IL);

        TituloEventoInformacion.setText(TitleEv);

        FechaEventoInformacion.setText(FormatDateInformation);
        DescripcionEventoInformacion.setText(DescriptionEv);


    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog DialogFragmentE = super.onCreateDialog(savedInstanceState);
        DialogFragmentE.requestWindowFeature(Window.FEATURE_NO_TITLE);
        DialogFragmentE.setCanceledOnTouchOutside(false);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
        DialogFragmentE.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        DialogFragmentE.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        DialogFragmentE.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        DialogFragmentE.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        DialogFragmentE.setCancelable(false);
        return DialogFragmentE;
    }


    public static EventModalInformation newInstance(String Image, String Title, String Date,
                                                    String Description){
        String ARG_FIRST_PARAM     = "image";
        String ARG_SECOND_PARAM    = "title";
        String ARG_THIRD_PARAM     = "date";
        String ARG_FOURTH_PARAM    = "description";
        EventModalInformation F = new EventModalInformation();
        Bundle B = new Bundle();
        B.putString(ARG_FIRST_PARAM,    Image);
        B.putString(ARG_SECOND_PARAM,   Title);
        B.putString(ARG_THIRD_PARAM,    Date);
        B.putString(ARG_FOURTH_PARAM,   Description);
        F.setArguments(B);
        return F;
    }

}