package com.elaniin.plazamalta.Eventos.Adapters;

import android.content.Context;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.view.LayoutInflater;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.Date;
import java.util.ArrayList;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.elaniin.plazamalta.R;
import com.elaniin.plazamalta.Utils.Core;
import com.elaniin.plazamalta.Utils.CustomV;
import com.elaniin.plazamalta.Eventos.Models.EventosModel;
import com.elaniin.plazamalta.Eventos.Fragments.EventModalInformation;

/**
 * Created by elaniin on 14/9/2017.
 */

public class EventosAdapter extends RecyclerView.Adapter<EventosAdapter.VH> {

    private View Vw;
    private Context ContextActivity;
    private ArrayList<EventosModel> EventosList = new ArrayList<>();

    public EventosAdapter(Context ContextActivity, ArrayList<EventosModel> EventosList){
        this.ContextActivity = ContextActivity;
        this.EventosList = EventosList;
    }

    @Override
    public EventosAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
        Vw = LayoutInflater.from(parent.getContext()).inflate(R.layout.eventos_item, parent, false);
        return new VH(Vw);
    }

    @Override
    public void onBindViewHolder(EventosAdapter.VH holder, int x) {
        final EventosModel EventoPosition = EventosList.get(x);

        ImageLoader IL = CustomV.getInstance(ContextActivity).getImageLoader();
        IL.get(EventoPosition.getEventoImage(), IL.getImageListener(holder.ImageEvent,
                R.drawable.header_tiendas, android.R.drawable.ic_dialog_alert), 100, 100);
        holder.ImageEvent.setImageUrl(EventoPosition.getEventoImage(),IL);

        holder.TextViewTitleEvent.setText(EventoPosition.getTituloEvento());


        Date DateInformation = null;
        try {
            DateInformation = new SimpleDateFormat("yyyy-MM-dd").parse(EventoPosition.getFechaEvento());
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        String FormatDateInformation = new SimpleDateFormat("EEE, MMM dd yyyy").format(DateInformation);
        holder.TextViewDate.setText(FormatDateInformation);
    }

    private Core PlazaMalta(){
        return Core.Companion.getInstance();
    }

    @Override
    public int getItemCount() {
        return EventosList.size();
    }

    class VH extends RecyclerView.ViewHolder {

        TextView            TextViewTitleEvent;
        TextView            TextViewDate;
        NetworkImageView    ImageEvent;

        VH(View iVw) {
            super(iVw);
            ImageEvent              = (NetworkImageView)    iVw.findViewById(R.id.ImageEvent);
            TextViewDate            = (TextView)            iVw.findViewById(R.id.TextViewDate);
            TextViewTitleEvent      = (TextView)            iVw.findViewById(R.id.TextViewTitleEvent);
            SetLatoFont();

            iVw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int x = getAdapterPosition();
                    if(x != RecyclerView.NO_POSITION) {
                        FragmentManager FM = ((FragmentActivity) ContextActivity).getSupportFragmentManager();
                        EventModalInformation E = EventModalInformation.newInstance(EventosList.get(getAdapterPosition()).getEventoImage(),
                                EventosList.get(getAdapterPosition()).getTituloEvento(),EventosList.get(getAdapterPosition()).getFechaEvento(),
                                EventosList.get(getAdapterPosition()).getDescripcionEvento());
                        FragmentTransaction FT = FM.beginTransaction();
                        FT.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        FT.add(android.R.id.content, E).addToBackStack(null).commit();
                    }
                }
            });

        }

        void SetLatoFont(){
            PlazaMalta().setLatoFont(TextViewDate,          "Regular");
            PlazaMalta().setLatoFont(TextViewTitleEvent,    "Regular");
        }

    }
}