package com.elaniin.plazamalta.Tiendas.Fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.*
import org.json.JSONObject



class Tiendas : Fragment() {

    var Vw : View ?= null

    var TiendasContext  : FragmentActivity ?= null
    var mListener       : OnFragmentInteractionListener? = null

    var InfoConnection              : ConstraintLayout  ?= null
    private var ProgressBarLoader           : ProgressBar?= null
    private var ProgressHeaderLoader        : ProgressBar ?= null



    var TAG_ID                      : String = Core.TAG_ID
    var TAG_NAME                    : String = "nombre"

    private var TPAdapter               : commerceAdapter?= null
    private var TiendasList         : ArrayList<commerceModel>   ?= null

    private var RecyclerViewInfo : RecyclerView?= null
    private var spinnerCat : Spinner?= null

    private val TAG = Tiendas::class.java!!.simpleName

    var service = ServiceRequest()
    val ApplicationC = ApplicationController(service)

    val data_id = ArrayList<String>()
    val data  = ArrayList<String>()


    override fun onCreate(savedInstanceState:Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw =  inflater!!.inflate(R.layout.fragment_tiendas, container, false)

        PlazaMalta().setToolbarHeader("¿Qué comprar?")
        InitFragmentElements(Vw!!)

        if(PlazaMalta().connectionExists(TiendasContext!!)){
            GetData()
        }else{
            InfoConnection?.visibility              = View.VISIBLE
        }



        return Vw
    }

    fun PlazaMalta() : Core {
        return Core.instance!!
    }

    fun InitFragmentElements(Vw : View){
        TiendasList = ArrayList()
        InfoConnection              = Vw.findViewById(R.id.InfoConnection)
        ProgressHeaderLoader        = Vw.findViewById(R.id.ProgressHeaderLoader)
        ProgressBarLoader           = Vw.findViewById(R.id.ProgressBarLoader)
        RecyclerViewInfo            = Vw.findViewById(R.id.RecyclerViewInfo)
        spinnerCat                  = Vw.findViewById(R.id.spinnerCat)
        InfoConnection?.visibility  = View.GONE

        TPAdapter = commerceAdapter(activity!!, TiendasList!!)

        ProgressHeaderLoader?.visibility = View.VISIBLE

        RecyclerViewInfo?.setHasFixedSize(true)
        val LManager = LinearLayoutManager(activity)
        RecyclerViewInfo?.layoutManager = LManager
        LManager.removeAllViews()
    }

    fun GetData() {
        ApplicationC.Get(Core.TAG_CATEGORIAS_TIENDAS) { response ->
            if (response != null) {
                for (a in 0 until response.length()) {
                    data_id.add(response.getJSONObject(a).getString(TAG_ID))
                    data.add(response.getJSONObject(a).getString(TAG_NAME))
                }
                if(isAdded) {
                    spinnerCat?.adapter = ArrayAdapter(activity, R.layout.spinner_text, data)
                    ProgressHeaderLoader?.visibility = View.GONE
                    ProgressBarLoader?.visibility = View.VISIBLE
                }
            } else {
                PlazaMalta().cancelPendingRequests(TAG)
                if(isAdded) {
                    if (PlazaMalta().connectionExists(TiendasContext!!)) {
                        Toast.makeText(TiendasContext, getString(R.string.ToastMessageNoConnection), Toast.LENGTH_LONG).show()
                    } else {//No hay conexión a internet.
                        InfoConnection?.visibility = View.VISIBLE
                    }
                    ProgressHeaderLoader?.visibility = View.GONE
                    ProgressBarLoader?.visibility = View.VISIBLE
                }
            }
        }

        spinnerCat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                RecyclerViewInfo?.visibility           = View.GONE
                ProgressBarLoader?.visibility           = View.VISIBLE
                TiendasList?.clear()
                ApplicationC.Get(Core.TAG_TIENDAS + data_id[i]) { response ->
                    if (response != null) {
                        (0 until response.length()).forEach { t ->
                            val TiendaJsonRequest: JSONObject = response.getJSONObject(t)
                            //Set de variables según el orden del json para el modelo: Tiendas.
                            val Horarios = TiendaJsonRequest.getJSONObject(Core.TAG_HORARIOS)
                            TiendasList?.add(commerceModel(
                                    TiendaJsonRequest.optString(Core.TAG_ID), "tienda",
                                    TiendaJsonRequest.optString(Core.TAG_TITULO),
                                    TiendaJsonRequest.optString(Core.TAG_DESCRIPCION_CORTA),
                                    TiendaJsonRequest.optString(Core.TAG_LOGO),
                                    TiendaJsonRequest.optString(Core.TAG_DESCRIPCION),
                                    TiendaJsonRequest.optString(Core.TAG_WEBSITE),
                                    TiendaJsonRequest.optString(Core.TAG_EMAIL),
                                    TiendaJsonRequest.optString(Core.TAG_FACEBOOK),
                                    TiendaJsonRequest.optString(Core.TAG_TWITTER),
                                    TiendaJsonRequest.optString(Core.TAG_INSTAGRAM),
                                    TiendaJsonRequest.optString(Core.TAG_TELEFONO),
                                    TiendaJsonRequest.optString(Core.TAG_TELEFONO_2),
                                    TiendaJsonRequest.optString(Core.TAG_WHATSAPP),
                                    "NULL",

                                    Horarios.optString(Core.TAG_A_LUNES),
                                    Horarios.optString(Core.TAG_C_LUNES),
                                    Horarios.optString(Core.TAG_A_MARTES),
                                    Horarios.optString(Core.TAG_C_MARTES),
                                    Horarios.optString(Core.TAG_A_MIERCOLES),
                                    Horarios.optString(Core.TAG_C_MIERCOLES),
                                    Horarios.optString(Core.TAG_A_JUEVES),
                                    Horarios.optString(Core.TAG_C_JUEVES),
                                    Horarios.optString(Core.TAG_A_VIERNES),
                                    Horarios.optString(Core.TAG_C_VIERNES),
                                    Horarios.optString(Core.TAG_A_SABADO),
                                    Horarios.optString(Core.TAG_C_SABADO),
                                    Horarios.optString(Core.TAG_A_DOMINGO),
                                    Horarios.optString(Core.TAG_C_DOMINGO)))

                        }
                        if(isAdded) {
                            ProgressBarLoader?.visibility = View.GONE
                            RecyclerViewInfo?.visibility           = View.VISIBLE

                            RecyclerViewInfo?.recycledViewPool?.clear()
                            RecyclerViewInfo?.adapter = TPAdapter

                            TPAdapter?.notifyDataSetChanged()
                        }

                    } else {

                        if(isAdded) {
                            PlazaMalta().cancelPendingRequests(TAG)
                            if (PlazaMalta().connectionExists(context!!)) {
                                Toast.makeText(context, getString(R.string.ToastMessageNoConnection), Toast.LENGTH_LONG).show()
                            } else {
                                //No hay conexión a internet.
                            }
                            ProgressBarLoader?.visibility = View.GONE
                        }

                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
    }

    override fun onAttach(context:Context?) {
        super.onAttach(context)
        TiendasContext = context as FragmentActivity
        if (context is OnFragmentInteractionListener) {
            mListener = context as OnFragmentInteractionListener?
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }


    override fun onDetach() {
        super.onDetach()
        mListener = null
    }



    interface OnFragmentInteractionListener { fun onFragmentInteraction(uri:Uri) }
    companion object {
        fun newInstance(): Tiendas { return Tiendas() }
    }

}