package com.elaniin.plazamalta.Restaurantes.Fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.*
import org.json.JSONObject


class Restaurantes : Fragment() {

    private var Vw : View ?= null

    private var RestaurantesContext : FragmentActivity ?= null
    private var mListener           : OnFragmentInteractionListener? = null

    private var RecyclerViewInformation : RecyclerView?= null
    private var spinnerCategories : Spinner ?= null
    private var RAdapter                    : commerceAdapter?= null
    private var RestaurantesList            : ArrayList<commerceModel>? = null
    private var ProgressBarLoader           : ProgressBar?= null
    private var ProgressHeaderLoader        : ProgressBar ?= null

    private var InfoConnection                  : ConstraintLayout  ?= null

    var TAG_ID      : String = Core.TAG_ID
    var TAG_NAME    : String = "nombre"
    val TAG_TYPE    : String = "restaurante"

    var TAG = Restaurantes::class.java.simpleName!!


    val data_id = ArrayList<String>()
    val data  = ArrayList<String>()

    //InitGlobaVariable
    var Service = ServiceRequest()
    val ApplicationC = ApplicationController(Service)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw = inflater!!.inflate(R.layout.fragment_restaurantes, container, false)

        PlazaMalta().setToolbarHeader("¿Qué comer?")
        InitFragmentElements(Vw!!)

        if(PlazaMalta().connectionExists(RestaurantesContext!!)){
            GetData()
        }else{
            InfoConnection?.visibility                  = View.VISIBLE
        }



        return Vw
    }

    fun PlazaMalta() : Core {
        return Core.instance!!
    }

    fun InitFragmentElements(Vw : View){
        InfoConnection              = Vw.findViewById(R.id.InfoConnection)
        ProgressHeaderLoader        = Vw.findViewById(R.id.ProgressHeaderLoader)
        ProgressBarLoader           = Vw.findViewById(R.id.ProgressBarLoader)
        RecyclerViewInformation     = Vw.findViewById(R.id.RecyclerViewInformation)
        spinnerCategories           = Vw.findViewById(R.id.spinnerCategories)
        RestaurantesList            = ArrayList()
        InfoConnection?.visibility  = View.GONE

        RAdapter = commerceAdapter(activity!!, RestaurantesList!!)

        ProgressHeaderLoader?.visibility = View.VISIBLE

        RecyclerViewInformation?.setHasFixedSize(true)
        val LManager = LinearLayoutManager(activity)
        RecyclerViewInformation?.layoutManager = LManager
        LManager.isAutoMeasureEnabled = false

    }


    fun GetData(){

        ApplicationC.Get(Core.TAG_CATEGORIAS_RESTAURANTES) { response ->
            if (response != null) {

                for (a in 0..response.length() - 1) {
                    data_id.add(response.getJSONObject(a).getString(TAG_ID))
                    data.add(response.getJSONObject(a).getString(TAG_NAME))
                }
                if(isAdded) {
                    spinnerCategories?.adapter = ArrayAdapter(activity, R.layout.spinner_text, data)
                }

                ProgressHeaderLoader?.visibility = View.GONE
                ProgressBarLoader?.visibility = View.VISIBLE

            }else{
                if(isAdded) {
                    PlazaMalta().cancelPendingRequests(TAG)
                    if (PlazaMalta().connectionExists(RestaurantesContext!!)) {
                        Toast.makeText(RestaurantesContext, getString(R.string.ToastMessageNoConnection), Toast.LENGTH_LONG).show()
                    } else {
                        //No hay conexión a internet.
                    }
                    ProgressHeaderLoader?.visibility = View.GONE
                    ProgressBarLoader?.visibility = View.VISIBLE
                }
            }
        }


        spinnerCategories?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                RecyclerViewInformation?.visibility = View.GONE
                ProgressBarLoader?.visibility = View.VISIBLE

                RestaurantesList?.clear()
                ApplicationC.Get(Core.TAG_RESTAURANTES + data_id[i]) { response ->
                    if (response != null) {
                        (0..response.length() - 1).forEach { t ->
                            val RestauranteJsonRequest: JSONObject = response.getJSONObject(t)
                            val Horarios = RestauranteJsonRequest.getJSONObject(Core.TAG_HORARIOS)//TAGObjectHorarios

                            RestaurantesList?.add(commerceModel(
                                    RestauranteJsonRequest.optString(Core.TAG_ID), "restaurante",
                                    RestauranteJsonRequest.optString(Core.TAG_TITULO),
                                    RestauranteJsonRequest.optString(Core.TAG_DESCRIPCION_CORTA),
                                    RestauranteJsonRequest.optString(Core.TAG_LOGO),
                                    RestauranteJsonRequest.optString(Core.TAG_DESCRIPCION),
                                    RestauranteJsonRequest.optString(Core.TAG_WEBSITE),
                                    RestauranteJsonRequest.optString(Core.TAG_EMAIL),
                                    RestauranteJsonRequest.optString(Core.TAG_FACEBOOK),
                                    RestauranteJsonRequest.optString(Core.TAG_TWITTER),
                                    RestauranteJsonRequest.optString(Core.TAG_INSTAGRAM),
                                    RestauranteJsonRequest.optString(Core.TAG_TELEFONO),
                                    RestauranteJsonRequest.optString(Core.TAG_TELEFONO_2),
                                    RestauranteJsonRequest.optString(Core.TAG_WHATSAPP),
                                    RestauranteJsonRequest.optString(Core.TAG_MENU_PDF),

                                    Horarios.optString(Core.TAG_A_LUNES),
                                    Horarios.optString(Core.TAG_C_LUNES),
                                    Horarios.optString(Core.TAG_A_MARTES),
                                    Horarios.optString(Core.TAG_C_MARTES),
                                    Horarios.optString(Core.TAG_A_MIERCOLES),
                                    Horarios.optString(Core.TAG_C_MIERCOLES),
                                    Horarios.optString(Core.TAG_A_JUEVES),
                                    Horarios.optString(Core.TAG_C_JUEVES),
                                    Horarios.optString(Core.TAG_A_VIERNES),
                                    Horarios.optString(Core.TAG_C_VIERNES),
                                    Horarios.optString(Core.TAG_A_SABADO),
                                    Horarios.optString(Core.TAG_C_SABADO),
                                    Horarios.optString(Core.TAG_A_DOMINGO),
                                    Horarios.optString(Core.TAG_C_DOMINGO)))
                        }
                        if(isAdded) {
                            ProgressBarLoader?.visibility = View.GONE
                            RecyclerViewInformation?.visibility = View.VISIBLE

                            RecyclerViewInformation?.recycledViewPool?.clear()
                            RecyclerViewInformation?.adapter = RAdapter
                            RAdapter?.notifyDataSetChanged()

                        }

                    }else{
                        if(isAdded) {
                            //PlazaMalta().cancelPendingRequests(TAG)
                            if (PlazaMalta().connectionExists(context!!)) {
                                Toast.makeText(activity, context!!.getString(R.string.ToastMessageNoConnection), Toast.LENGTH_LONG).show()
                            } else {
                                //No hay conexión a internet.
                            }
                            ProgressBarLoader?.visibility = View.GONE
                        }

                    }
                }

            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {}
        }


    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        RestaurantesContext = context as FragmentActivity
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener { fun onFragmentInteraction(uri: Uri) }
    companion object {
        fun newInstance(): Restaurantes { return Restaurantes() }
    }

}