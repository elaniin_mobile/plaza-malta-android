package com.elaniin.plazamalta.Restaurantes.Fragments

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.DialogFragment

import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.webkit.WebView
import android.widget.ImageButton

import com.elaniin.plazamalta.R



class Menu : DialogFragment() {

    private var mParamMenu  : String    ?= null
    private var mWebView    : WebView   ?= null
    private var mView       : View      ?= null
    private var mButtonClose : ImageButton ?= null

    private var mConstraintClose : ConstraintLayout?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParamMenu = arguments!!.getString(ARG_MENU)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_menu, container, false)

        mWebView = mView?.findViewById(R.id.web_view_menu)
        mConstraintClose = mView?.findViewById(R.id.constraint_close_icon)
        mButtonClose = mView?.findViewById(R.id.image_button_close)

        mConstraintClose?.setOnClickListener { dismiss() }
        mButtonClose?.setOnClickListener { dismiss() }

        mWebView?.settings?.javaScriptEnabled = true
        mWebView?.loadUrl("https://docs.google.com/viewer?url=$mParamMenu")
        return mView
    }

    companion object {
        private val ARG_MENU = "menu_url"
        fun newInstance(menu_value : String): Menu {
            val fragment = Menu()
            val args = Bundle()
            args.putString(ARG_MENU, menu_value)
            fragment.arguments = args
            return fragment
        }
    }
}