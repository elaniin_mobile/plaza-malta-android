package com.elaniin.plazamalta.Ubicacion

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Utils.Core


class Ubicacion : Fragment(), OnMapReadyCallback {

    private var Vw                  : View      ?= null
    private var mView               : MapView   ?= null
    private var mGoogleMap          : GoogleMap ?= null
    private var WazeButton          : Button    ?= null
    private var GoogleMapButton     : Button    ?= null
    private var mListener           : OnFragmentInteractionListener? = null

    private var PATH_WAZE_APP       : String = "market://details?id=com.waze"
    private var TAG_URL_WAZE        : String = "https://waze.com/ul?ll=13.66713826&-89.27078938"
    private var TAG_URI_GMAPS       : String = "http://maps.google.com/maps?q=loc:13.66713826,-89.27078938 (Plaza Malta)"


    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Vw = inflater!!.inflate(R.layout.fragment_ubicacion, container, false)

        PlazaMalta().setToolbarHeader("Ubicación")

        FragmentButtonActions()

        return Vw
    }

    fun InitFragmentElements(){
        WazeButton      = Vw?.findViewById(R.id.WazeButton)
        GoogleMapButton = Vw?.findViewById(R.id.GoogleMapButton)
    }

    fun FragmentButtonActions(){
        InitFragmentElements()
        GoogleMapButton?.setOnClickListener {

            val URI = TAG_URI_GMAPS
            val I = Intent(Intent.ACTION_VIEW, Uri.parse(URI))
            I.flags = Intent.FLAG_ACTIVITY_FORWARD_RESULT
            I.flags = Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP
            I.data = Uri.parse(URI)
            startActivity(I)
        }

        WazeButton?.setOnClickListener {
            try {
                val URL = TAG_URL_WAZE
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(URL))
                startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(PATH_WAZE_APP))
                startActivity(intent)
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = Vw?.findViewById(R.id.map)
        if(mView != null){
            mView?.onCreate(null)
            mView?.onResume()
            mView?.getMapAsync(this)
        }
    }

    override fun onMapReady(GM: GoogleMap?) {
        MapsInitializer.initialize(context)
        GM?.mapType = GoogleMap.MAP_TYPE_NORMAL
        val x : LatLng = LatLng(13.66713826,-89.27078938)
        GM?.addMarker(MarkerOptions().position(x).title("Plaza Malta."))
        val CX : CameraPosition = CameraPosition.builder().target(x).zoom(16F).bearing(0F).tilt(45F).build()
        GM?.moveCamera(CameraUpdateFactory.newCameraPosition(CX))
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        fun newInstance(): Ubicacion { return Ubicacion() }
    }

    private fun PlazaMalta(): Core {
        return Core.instance!!
    }
}