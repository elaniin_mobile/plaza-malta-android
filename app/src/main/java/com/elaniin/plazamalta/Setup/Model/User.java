package com.elaniin.plazamalta.Setup.Model;

/**
 * Created by elaniin on 25/9/2017.
 */


//Modelo para la información del usuario en FirebaseDatabase.
public class User {

    public String username;
    public String email;
    public String birthday;
    public String photo;

    public User(String username, String email, String birthday, String photo){
        this.username = username;
        this.email = email;
        this.birthday = birthday;
        this.photo = photo;
    }

    public User(){
    }

}
