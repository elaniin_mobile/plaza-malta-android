package com.elaniin.plazamalta.Setup


import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.elaniin.plazamalta.Login.Login
import com.elaniin.plazamalta.R
import com.elaniin.plazamalta.Setup.Model.User
import com.elaniin.plazamalta.Utils.Core
import kotlinx.android.synthetic.main.activity_setup.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class Setup : AppCompatActivity() {

    private val TAG = "Setup"
    private val REQUIRED = "Required"

    var FLAG                : Int = 0
    var xFLAG                : Int = 0
    var EmailText           : EditText ?= null
    var ShowPassword        : ImageView?= null
    var UserNameText        : EditText ?= null
    var BirthdayText        : TextView ?= null
    var PasswordText        : EditText ?= null
    var TextViewPassword    : TextView ?= null
    var TextFirstQuestion   : TextView ?= null
    var TextSecondQuestion  : TextView ?= null

    var mProgressBar        : ProgressBar ?= null

    var a : Activity ?= null

    var usr                 : FirebaseUser ?= null

    private var mAuth: FirebaseAuth? = null
    private var FirebaseAuthListener : FirebaseAuth.AuthStateListener ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)

        a = this

        InitActivityElements()
        SetProperties()
        SetLatoFont()
    }

    private fun InitActivityElements(){
        EmailText           = findViewById(R.id.EmailText)
        mProgressBar        = findViewById(R.id.progressBarLoadUser)
        ShowPassword        = findViewById(R.id.ShowPassword)
        PasswordText        = findViewById(R.id.PasswordText)
        BirthdayText        = findViewById(R.id.BirthdayText)
        UserNameText        = findViewById(R.id.UserNameText)
        TextViewPassword    = findViewById(R.id.TextViewPassword)
        TextFirstQuestion   = findViewById(R.id.TextFirstQuestion)
        TextSecondQuestion  = findViewById(R.id.TextSecondQuestion)

        ContSetup?.isFocusable = true
        ContSetup?.isFocusableInTouchMode = true

        val c = Calendar.getInstance()

        BirthdayText?.setOnClickListener {
            DatePickerDialog(a, DatePickerDialog.OnDateSetListener { datePicker, i, i1, i2 ->
                val m = arrayOf("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre")
                val x = SimpleDateFormat("dd/MM/yyyy").format(GregorianCalendar(i, i1, i2).timeInMillis).split("/")
                val d = "${x[0]} de ${m[Integer.parseInt(x[1]) - 1]} del ${x[2]}"
                BirthdayText?.text = d
            }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show()
        }

        ShowPassword?.setOnClickListener {
            when(FLAG){
                0 -> {
                    PasswordText?.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    FLAG += 1 }
                1 -> {
                    PasswordText?.transformationMethod = PasswordTransformationMethod.getInstance()
                    FLAG = 0 }
            }
        }

        show_password_confirmation.setOnClickListener {
            when(xFLAG){
                0 -> {
                    password_confirmation?.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    xFLAG += 1 }
                1 -> {
                    password_confirmation?.transformationMethod = PasswordTransformationMethod.getInstance()
                    xFLAG = 0 }
            }
        }

        mAuth = FirebaseAuth.getInstance()

        FirebaseAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) { }
        }

    }

    fun SetLatoFont(){
        PlazaMalta().setLatoFont(TextFirstQuestion!!,   "Bold")
        PlazaMalta().setLatoFont(TextSecondQuestion!!,  "Bold")
        PlazaMalta().setLatoFont(TextViewPassword!!,    "Bold")
    }

    override fun onStart(){
        super.onStart()
        mAuth?.addAuthStateListener(FirebaseAuthListener!!)
    }

    override fun onStop(){
        super.onStop()
        mAuth?.removeAuthStateListener(FirebaseAuthListener!!)
    }


    private fun SetProperties(){
        //FirstStep
        BackButtonFirstStep?.setOnClickListener { view ->
            startActivity(Intent(this@Setup, Login::class.java))
            finish()
        }
        NextButtonFirstStep?.setOnClickListener {
            if (UserNameText?.text.isNullOrEmpty() || UserNameText?.text.isNullOrEmpty()) {
                Toast.makeText(this@Setup, applicationContext.getString(R.string.MessageBlankUsernameText), Toast.LENGTH_LONG).show()
            }else if(BirthdayText?.text.isNullOrBlank() || BirthdayText?.text == getString(R.string.HintDateTextSetup)) {
                Toast.makeText(this@Setup, getString(R.string.MessageBlankBirthdayText), Toast.LENGTH_LONG).show()
            }else{
                FirstStepSetup?.visibility      = View.GONE
                SecondStepSetup?.visibility     = View.VISIBLE
                SecondStepButton?.visibility    = View.VISIBLE
            }
        }

        //SecondStep
        BackButtonSecondStep?.setOnClickListener {
            FirstStepSetup?.visibility      = View.VISIBLE
            SecondStepSetup?.visibility     = View.GONE
            SecondStepButton?.visibility    = View.GONE
        }

        NextButtonSecondStep?.setOnClickListener {


            val matcherObj: Matcher = Pattern.compile(Core.EmailValidation).matcher(EmailText?.text.toString())


                        if (!matcherObj.matches()) {
                Toast.makeText(this@Setup, getString(R.string.MessageBlankEmailText), Toast.LENGTH_LONG).show()
            }else if (EmailText?.text.isNullOrEmpty() || EmailText?.text.isNullOrEmpty()) {
                Toast.makeText(this@Setup, getString(R.string.MessageBlankEmailText), Toast.LENGTH_LONG).show()
            }else{
                ThirdStepSetup?.visibility  = View.VISIBLE
                ThirdStepButton?.visibility = View.VISIBLE
                SecondStepSetup?.visibility = View.GONE
            }

        }



        //ThirdStep
        BackButtonThirdStep?.setOnClickListener {
            ThirdStepSetup?.visibility  = View.GONE
            ThirdStepButton?.visibility = View.GONE
            SecondStepSetup?.visibility = View.VISIBLE
        }
        NextButtonThirdStep?.setOnClickListener {
            mProgressBar?.visibility = View.VISIBLE
            if(PlazaMalta().connectionExists(applicationContext)) {
                when {
                    PasswordText?.text?.length!! <= 8 -> {
                        mProgressBar?.visibility = View.GONE
                        Toast.makeText(this@Setup, applicationContext.getString(R.string.MessageValidPasswordLength), Toast.LENGTH_LONG).show()
                    }
                    password_confirmation.text.toString() != PasswordText?.text.toString()  -> {
                        mProgressBar?.visibility = View.GONE
                        Toast.makeText(this@Setup, applicationContext.getString(R.string.MessagePasswordValidation), Toast.LENGTH_LONG).show()
                    }
                    else -> {
                        mProgressBar?.visibility = View.GONE
                        mAuth?.createUserWithEmailAndPassword(EmailText?.text.toString(), PasswordText?.text.toString())?.addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                usr = task.result.user
                                val user = mAuth?.currentUser
                                Stat(user!!, 1)
                                mProgressBar?.visibility = View.GONE
                            } else {
                                Toast.makeText(this@Setup, applicationContext.getString(R.string.MessageValidStatusFirebase), Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
            }else{
                mProgressBar?.visibility = View.GONE
                Toast.makeText(this@Setup, applicationContext.getString(R.string.MessageNoConnection), Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_UP) {
            val view = currentFocus
            if (view != null) {
                val consumed = super.dispatchTouchEvent(ev)
                val viewTmp = currentFocus
                val viewNew = viewTmp ?: view
                if (viewNew.equals(view)) {
                    val rect = Rect()
                    val coordinates = IntArray(2)
                    view.getLocationOnScreen(coordinates)
                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.width, coordinates[1] + view.height)
                    val x = ev.x.toInt()
                    val y = ev.y.toInt()
                    if (rect.contains(x, y)) {
                        return consumed
                    }
                } else if (viewNew is EditText) {
                    return consumed
                }
                val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(viewNew.windowToken, 0)
                viewNew.clearFocus()
                return consumed
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun Stat(user : FirebaseUser, status : Int){
        when (status){
            2 -> { }
            1 -> {
                CreateUserDatabase(user.uid)
                Toast.makeText(this@Setup, applicationContext.getString(R.string.WelcomeMessageSetup), Toast.LENGTH_LONG).show()
                FirebaseAuth.getInstance().signOut()
                LoginManager.getInstance().logOut()
                var i : Intent = Intent(this@Setup, Login::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
            }
        }
    }


    private fun CreateUserDatabase(userID : String){
        val StorageFirebase = FirebaseStorage.getInstance()
        val ReferenceStorage = StorageFirebase.getReferenceFromUrl("gs://plazamalta-app.appspot.com")
        val mDatabase = FirebaseDatabase.getInstance().reference
        val PhotoPath = "https://firebasestorage.googleapis.com/v0/b/plazamalta-app.appspot.com/o/ProfilePicture%2FInitImage%2Fprofile_avatar.png?alt=media&token=45f0208b-69d8-40a9-a3af-6a136637702a"
        val UserInformation : User = User(UserNameText?.text.toString(),EmailText?.text.toString(),BirthdayText?.text.toString(),PhotoPath)
        mDatabase.child("users").child(userID).setValue(UserInformation)
    }


    fun PlazaMalta() : Core {
        return Core.instance!!
    }

}