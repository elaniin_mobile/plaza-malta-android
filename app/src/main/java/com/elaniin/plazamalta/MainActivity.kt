package com.elaniin.plazamalta

import android.net.Uri
import android.os.Bundle
import android.app.AlertDialog
import android.content.Intent

import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.app.FragmentManager
import android.support.design.widget.NavigationView
import android.support.v4.content.res.ResourcesCompat

import android.support.v7.app.AppCompatActivity
import android.support.v7.app.ActionBarDrawerToggle

import android.view.View
import android.view.MenuItem

import android.widget.Toast
import android.widget.TextView

import com.bumptech.glide.Glide
import com.android.volley.Response

import com.elaniin.plazamalta.Utils.Core
import com.elaniin.plazamalta.Login.Login
import com.elaniin.plazamalta.Ajustes.Ajustes
import com.elaniin.plazamalta.Setup.Model.User
import com.elaniin.plazamalta.Ubicacion.Ubicacion
import com.elaniin.plazamalta.Search.Fragments.Search
import com.elaniin.plazamalta.Tiendas.Fragments.Tiendas
import com.elaniin.plazamalta.Eventos.Fragments.Eventos
import com.elaniin.plazamalta.Promociones.Fragments.Promociones
import com.elaniin.plazamalta.Restaurantes.Fragments.Restaurantes

import com.google.firebase.database.*
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.Auth
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.crash.FirebaseCrash
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, Tiendas.OnFragmentInteractionListener,
        Promociones.OnFragmentInteractionListener, Eventos.OnFragmentInteractionListener, Restaurantes.OnFragmentInteractionListener,
        Ubicacion.OnFragmentInteractionListener, GoogleApiClient.OnConnectionFailedListener, Ajustes.OnFragmentInteractionListener{

    private var mUserName               : TextView ?= null
    private var mAvatarUser             : CircleImageView ?= null
    private var mHeaderNavigationView   : View ?= null

    private var mFirebaseUser           : FirebaseUser ?= null
    private var mFirebaseAuth           : FirebaseAuth ?= null
    private var mGoogleApiClient        : GoogleApiClient? = null
    private var mFirebaseListener       : FirebaseAuth.AuthStateListener ?= null
    private var mDatabase               : DatabaseReference?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        toolbar.post({
            val d = ResourcesCompat.getDrawable(resources, R.drawable.ic_menu_, null)
            toolbar.navigationIcon = d
        })

        nav_view.setNavigationItemSelectedListener(this)

        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mHeaderNavigationView   = nav_view.inflateHeaderView(R.layout.nav_header_main)

        mUserName               = mHeaderNavigationView?.findViewById(R.id.text_user_name_header)
        mAvatarUser             = mHeaderNavigationView?.findViewById(R.id.avatar_user_header)

        val toggle = ActionBarDrawerToggle(
                this,
                drawer_layout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)

        drawer_layout.addDrawerListener(toggle)

        toggle.syncState()
        FirebaseCrash.report(Exception())

        var mGoogleOptions : GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_server_client_id))
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleOptions)
                .build()

        LogoutCont.setOnClickListener {
            val S = AlertDialog.Builder(this@MainActivity)
            S.setMessage(applicationContext.getString(R.string.MessageLogoutDialog))
            S.setCancelable(false)
            S.setPositiveButton("SI", { dialog, which ->
                if(PlazaMalta().connectionExists(applicationContext)) {
                    Logout()
                }else{
                    Toast.makeText(this@MainActivity, applicationContext.getString(R.string.MessageNoConnection), Toast.LENGTH_SHORT).show()
                }
            })
            S.setNegativeButton("NO", { dialog, which -> dialog.cancel() })
            val AlertSendData = S.create()
            AlertSendData.show()
        }

        FacebookAccess.setOnClickListener {
            val FacebookID = "1399924070039423"
            val FacebookURL = "https://www.facebook.com/PlazaMalta"
            val versionCode : Int = applicationContext.packageManager.getPackageInfo("com.facebook.katana", 0).versionCode
            if(!FacebookID.isEmpty()) {
                // open the Facebook PlazaMalta using facebookID (fb://profile/facebookID or fb://page/facebookID)
                val uri: Uri = Uri.parse("fb://page/" + FacebookID)
                startActivity(Intent(Intent.ACTION_VIEW, uri))
            } else if (versionCode >= 3002850 && ! FacebookURL.isEmpty()) {
                // open Facebook PlazaMalta using facebook url
                val uri : Uri = Uri.parse("fb://facewebmodal/f?href=" + FacebookURL)
                startActivity(Intent(Intent.ACTION_VIEW, uri))
            } else {
                // Facebook is not installed. Open the browser
                val uri : Uri = Uri.parse(FacebookURL)
                startActivity(Intent(Intent.ACTION_VIEW, uri))
            }
        }



        InstagramAccess.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("http://instagram.com/_u/plazamalta")
                intent.`package` = "com.instagram.android"
                startActivity(intent)
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/plazamalta")))
            }
        }
        mFirebaseAuth = FirebaseAuth.getInstance()
        mFirebaseListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            mFirebaseUser = firebaseAuth.currentUser
            if (mFirebaseUser != null) {
                for (profile in mFirebaseUser?.providerData!!) {
                    if(profile.providerId == "password"){
                        Glide.with(this).load(R.drawable.profile_avatar).into(mAvatarUser)
                        mDatabase = FirebaseDatabase.getInstance().reference.child("users").child(mFirebaseUser?.uid)
                        val postListener = object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val post = dataSnapshot.getValue<User>(User::class.java)
                                mUserName?.text = post.username
                                Glide.with(this@MainActivity).load(post.photo).into(mAvatarUser)
                            }
                            override fun onCancelled(databaseError: DatabaseError) {}
                        }
                        mDatabase?.addValueEventListener(postListener)
                    }else{
                        Glide.with(this).load(mFirebaseUser?.photoUrl).into(mAvatarUser)
                        mUserName?.text = profile.displayName
                    }
                }
            }else{
                returnLogin()
            }
        }
        setProperties()
        SetLatoFont()
    }




    fun setProperties(){

        PlazaMalta().setOnTitleChange(Response.Listener { response ->
            TextToolbar.visibility = View.GONE
            if (!(response[0] as Boolean)) {
                TextToolbar.visibility = View.VISIBLE
                TextToolbar.text = response[1].toString()
            }
            invalidateOptionsMenu()
        })


        PlazaMalta().setlogoVisibility(Response.Listener { response ->
            if(response){
                logo_actionbar.visibility = View.VISIBLE
            }else{
                logo_actionbar.visibility = View.GONE
            }
        })

        PlazaMalta().setOnFragmentLocationReset(Response.Listener {
            supportFragmentManager.popBackStack("MainActivity", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            supportFragmentManager.beginTransaction().replace(R.id.FragmentsContainer, Tiendas.newInstance()).commit()
        })
        PlazaMalta().setOnFragmentChange(Response.Listener { response ->
            supportFragmentManager.beginTransaction().replace(R.id.FragmentsContainer, response).addToBackStack("MainActivity").commit()
        })
        PlazaMalta().setMainFragment()
    }

     private fun PlazaMalta(): Core {
        return Core.instance!!
    }

    fun SetLatoFont() {
        PlazaMalta().setLatoFont(TextToolbar, Core.BOLD_FONT_TYPE)
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun returnLogin(){
        val i = Intent(this@MainActivity, Login::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
    }

    private fun Logout(){
        FirebaseAuth.getInstance().signOut()
        LoginManager.getInstance().logOut()
        returnLogin()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.i1 -> PlazaMalta().setMainFragment() //TiendasLikeMainFragment
            R.id.i2 -> PlazaMalta().setFragment(Restaurantes.newInstance()) //RestaurantesFragment
            R.id.i3 -> PlazaMalta().setFragment(Promociones.newInstance()) //PromocionesFragment
            R.id.i4 -> PlazaMalta().setFragment(Eventos.newInstance()) //EventosFragment
            R.id.i5 -> PlazaMalta().setFragment(Search.newInstance()) //SearchFragment
            R.id.i6 -> PlazaMalta().setFragment(Ajustes.newInstance()) //AjustesFragment
            R.id.i7 -> PlazaMalta().setFragment(Ubicacion.newInstance()) //UbicacionSecion
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onFragmentInteraction(uri: Uri) {}

    override fun onStart(){
        super.onStart()
        mFirebaseAuth?.addAuthStateListener(mFirebaseListener!!)
    }

    override fun onStop(){
        super.onStop()
        if(mFirebaseListener != null){
            mFirebaseAuth?.removeAuthStateListener(mFirebaseListener!!)
        }
    }

}